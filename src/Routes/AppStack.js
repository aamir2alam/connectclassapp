import React, { Component } from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { createStackNavigator, createDrawerNavigator, createBottomTabNavigator } from "react-navigation";
import Profile from "../component/profile/Profile";
import Notifications from "../component/notification/Notifications";
import HomePage from "../component/homePage/HomePage";
import DrawerScreen from "../component/common/DrawerScreen";
import BookMarked from "../component/bookmarked/BookMarked";
import CreateAbout from "../component/profile/editpanels/CreateAbout";
import OtherProfile from "../component/profile/OtherProfile";
import { Badge, Text } from "native-base";
import { View } from "react-native";
import commonColor from "../../native-base-theme/variables/commonColor";
import WithBadge from "../component/common/WithBadge";

const HomeStack = createStackNavigator(
  {
    HomePage: {
      screen: HomePage
    },
    OtherProfile: {
      screen: OtherProfile
    }
  },
  {
    headerMode: "none"
  }
);

HomeStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const NotificationStack = createStackNavigator(
  {
    NotificationPage: {
      screen: Notifications
    },
    OtherProfile: {
      screen: OtherProfile
    }
  },
  {
    headerMode: "none"
  }
);

NotificationStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const ProfileStack = createStackNavigator(
  {
    ProfilePage: {
      screen: Profile
    },
    EditPage: {
      screen: CreateAbout
    }
  },
  {
    headerMode: "none"
  }
);

ProfileStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const TabNavigation = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name="home-outline" size={24} />
      }
    },
    Notification: {
      screen: NotificationStack,
      navigationOptions: {
        tabBarLabel: "Notification",
        title: "Notification",
        tabBarIcon: ({ tintColor }) => {
          return <WithBadge tintColor={tintColor} />;
        }
      }
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarLabel: "Profile",
        title: "Profile",
        tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name="account-outline" size={24} />
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "#0077b5",
      inactiveTintColor: "#212121"
    },
    initialRouteName: "Home",
    order: ["Home", "Notification", "Profile"]
  }
);

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: TabNavigation
    }
  },
  {
    initialRouteName: "Home",
    contentComponent: DrawerScreen,
    drawerWidth: 200,
    drawerPosition: "left",
    drawerLockMode: "locked-closed"
  }
);

const StackNavigator = createStackNavigator(
  {
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.

    DrawerNavigator: {
      screen: DrawerNavigator
    },
    BookMarked: {
      screen: BookMarked
    }
  },
  {
    headerMode: "none"
  }
);

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  headMargin: {
    marginTop: 80
  },
  card: {
    height: 200
  }
};

export default StackNavigator;
