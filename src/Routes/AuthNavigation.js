import { createSwitchNavigator, createStackNavigator, createAppContainer, withNavigation } from "react-navigation";
import Splash from "../component/Splash";
import StackNavigator from "./AppStack";
import AuthStack from "./AuthStack";

// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.

// const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen });
const AuthLoading = createStackNavigator({ AuthLoading: Splash }, { headerMode: "none" });

const AuthNavigation = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoading,
      App: StackNavigator,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);

export default AuthNavigation;
