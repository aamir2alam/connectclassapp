import LoginForm from "../component/LoginForm";
import { createStackNavigator } from "react-navigation";

const AuthStack = createStackNavigator(
  {
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.

    Login: {
      screen: LoginForm
    }
  },
  {
    headerMode: "none"
  }
);

export default AuthStack;
