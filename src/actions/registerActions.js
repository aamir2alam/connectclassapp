import axios from "axios";
import { baseurl } from "../utils/baseUrl";

export const getInstitutionSuggestions = param_options => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${baseurl}/api/institution/autocomplete`, { params: param_options })
      .then(res => {
        console.log(res);
        resolve(res.data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
};
