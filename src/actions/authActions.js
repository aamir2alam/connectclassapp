import { GET_ERRORS, SET_CURRENT_USER, LOGIN_LOADING, LOGIN_LOADING_FALSE, CLEAR_ERRORS } from "./types";
import axios from "axios";
import { Alert } from "react-native";
import setAuthToken from "../utils/setAuthTokens";
import jwt_decode from "jwt-decode";
import { baseurl } from "../utils/baseUrl";
import { AsyncStorage } from "react-native";
import { clearCurrentProfile } from "../actions/profileActions";
//Register USer

export const registerUser = userData => dispatch => {
  // dispatch(setLoginLoading());
  axios
    .post(baseurl + "/api/users/register", userData)
    .then(res => {})
    .catch(err => {
      dispatch(stopLoginLoading());
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

//Login -get user token

export const loginUser = userData => dispatch => {
  dispatch(setLoginLoading());
  axios
    .post(baseurl + "/api/users/login", userData)
    .then(res => {
      //Save to local storage
      const { token } = res.data;
      //Set token to local storage
      //TODO: localstorage setup for react native
      // to set
      // {accessible: ACCESSIBLE.WHEN_UNLOCKED} -> This for IOS
      AsyncStorage.setItem("jwtToken", token).then(
        res => {
          //localStorage.setItem("jwtToken", token);
          //Set token to auth header
          setAuthToken(token);
          //Decode token to get user data
          const decoded = jwt_decode(token);
          //Set current user
          dispatch(setCurrentUser(decoded));
        },
        err => {
          console.log("Error saving token ", err);
        }
      );
    })
    .catch(err => {
      dispatch(stopLoginLoading());
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getAuthToken = () => dispatch => {
  AsyncStorage.getItem("jwtToken")
    .then(token => {
      setAuthToken(token);
      // Returns a direct value
      const decoded = jwt_decode(token);
      dispatch(setCurrentUser(decoded));

      // Check for expired Token
      const currentTime = Date.now() / 1000;

      if (decoded.exp < currentTime) {
        //Logout user
        Alert.alert("Logged out", "Session expired please login again", [{ text: "OK" }]);
        dispatch(logoutUser());
        // // TODO: Clear current profile
        // dispatch(clearCurrentProfile());
      }
    })
    .catch(err => {
      dispatch(setCurrentUser({}));
    });
};

//Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

//Logout user
export const logoutUser = () => dispatch => {
  //Remove token
  //TODO: localstorage setup for react native
  // localStorage.removeItem("jwtToken");
  //Rmeove auth header for future requests
  AsyncStorage.removeItem("jwtToken")
    .then(token => {
      setAuthToken(false);
      //Set current user to {} which will set isAuthenticated to false
      dispatch(setCurrentUser({}));
    })
    .catch(err => {
      console.log("Error while removing token ", err);
    });
};

// present and dismiss loading during async tasks

export const setLoginLoading = () => dispatch => {
  dispatch({
    type: LOGIN_LOADING
  });
};

export const clearAuthErrors = () => dispatch => {
  dispatch({
    type: CLEAR_ERRORS
  });
};

export const stopLoginLoading = () => dispatch => {
  dispatch({
    type: LOGIN_LOADING_FALSE
  });
};
