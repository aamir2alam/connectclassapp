import axios from "axios";

import {
  GET_PROFILE,
  PROFILE_LOADING,
  GET_ERRORS,
  CLEAR_CURRENT_PROFILE,
  PROFILE_LOADING_FALSE,
  PROFILE_LOADING_2,
  PROFILE_LOADING_FALSE_2,
  GET_UPLOAD_PROGRESS
} from "./types";
import { baseurl } from "../utils/baseUrl";

//Get current profile
export const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get(`${baseurl}/api/profile`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err => {
      dispatch({
        type: GET_PROFILE,
        payload: {}
      });
    });
};

//Create Profile

export const createProfile = (profileData, history) => dispatch => {
  dispatch(setProfileLoading2());
  axios
    .post(`${baseurl}/api/profile`, profileData, {
      onUploadProgress: ProgressEvent => {
        // console.log((ProgressEvent.loaded * 100) / ProgressEvent.total);
        dispatch({
          type: GET_UPLOAD_PROGRESS,
          payload: Math.floor((ProgressEvent.loaded * 100) / ProgressEvent.total)
        });
      }
    })
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// export const updateProfileWithImage = (profileData) => dispatch => {
//   RNFetchBlob.fetch('POST', 'http://www.example.com/upload-form', {
//     Authorization : "Bearer access-token",
//     otherHeader : "foo",
//     'Content-Type' : 'multipart/form-data',
//   }, [
//     // element with property `filename` will be transformed into `file` in form data
//     { name : 'avatar', filename : 'avatar.png', data: binaryDataInBase64},
//     // custom content type
//     { name : 'avatar-png', filename : 'avatar-png.png', type:'image/png', data: binaryDataInBase64},
//     // part file from storage
//     { name : 'avatar-foo', filename : 'avatar-foo.png', type:'image/foo', data: RNFetchBlob.wrap(path_to_a_file)},
//     // elements without property `filename` will be sent as plain text
//     { name : 'name', data : 'user'},
//     { name : 'info', data : JSON.stringify({
//       mail : 'example@example.com',
//       tel : '12345678'
//     })},
//   ]).then((resp) => {
//     // ...
//   }).catch((err) => {
//     // ...
//   })
// }

export const updateVerification = verificationData => dispatch => {
  axios
    .post(`${baseurl}/api/users/update_verification`, verificationData)
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const addPortfolio = portfolioData => dispatch => {
  axios.post(`${baseurl}/api/profile/portfolio`, portfolioData).then(res => {
    dispatch({
      type: GET_PROFILE,
      payload: res.data
    });
  });
};

export const deletePortfolioItem = portfolioID => dispatch => {
  axios.delete(`${baseurl}/api/profile/portfolio/${portfolioID}`).then(res => {
    dispatch({
      type: GET_PROFILE,
      payload: res.data
    });
  });
};

//Add following
export const addFollowing = profileID => dispatch => {
  axios
    .post(`${baseurl}/api/profile/add_following/${profileID}`)
    .then(res => console.log(res))
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING
  };
};

export const setProfileStopLoading = () => {
  return {
    type: PROFILE_LOADING_FALSE
  };
};

export const setProfileLoading2 = () => {
  return {
    type: PROFILE_LOADING_2
  };
};

export const setProfileStopLoading2 = () => {
  return {
    type: PROFILE_LOADING_FALSE_2
  };
};

//clear profile
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  };
};

export const clearErrors = () => {
  return {
    type: GET_ERRORS,
    payload: {}
  };
};
