import axios from "axios";
import {
  GET_PROFILE_OTHER,
  GET_PROFILES,
  CLEAR_CURRENT_PROFILES,
  GET_ERRORS,
  PROFILE_LOADING_OTHER,
  PROFILE_LOADING_FALSE_OTHER,
  PROFILE_LOADING_2_OTHER,
  PROFILE_LOADING_FALSE_2_OTHER,
  CLEAR_CURRENT_PROFILE_OTHER
} from "./types";
import { baseurl } from "../utils/baseUrl";

//Get profile by handle
export const getProfileByHandle = handle => dispatch => {
  dispatch(setProfileLoadingOther());
  dispatch(clearCurrentProfileOther());
  axios
    .get(`${baseurl}/api/profile/handle/${handle}`)
    .then(res => {
      dispatch({
        type: GET_PROFILE_OTHER,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Get profile by id
export const getProfileById = profile_id => dispatch => {
  dispatch(setProfileLoadingOther());
  dispatch(clearCurrentProfileOther());
  axios
    .get(`${baseurl}/api/profile/${profile_id}`)
    .then(res => {
      dispatch({
        type: GET_PROFILE_OTHER,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Get profile by handle
export const getProfileSuggestion = () => dispatch => {
  dispatch(setProfileLoadingOther2());
  axios
    .get(`${baseurl}/api/profile/suggestions`)
    .then(res => {
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getProfileClassmates = () => dispatch => {
  dispatch(setProfileLoadingOther2());
  axios
    .get(`${baseurl}/api/profile/classmates/profiles`)
    .then(res => {
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Get profile by handle
export const getFollowingList = () => dispatch => {
  dispatch(setProfileLoadingOther2());
  axios
    .get(`${baseurl}/api/profile/get_followed/10/0`)
    .then(res => {
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Get profile by handle
export const getFollowersList = () => dispatch => {
  dispatch(setProfileLoadingOther2());
  axios
    .get(`${baseurl}/api/profile/get_follower/10/0`)
    .then(res => {
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Get profile by params
export const getProfilesbyQuery = (param_options, page_number) => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${baseurl}/api/profile/find/${page_number}`, { params: param_options })
      .then(res => {
        dispatch({
          type: GET_PROFILES,
          payload: res.data
        });
        resolve(res.data);
      })
      .catch(err => {
        dispatch({
          type: GET_PROFILES,
          payload: {}
        });
        reject(err);
      });
  });
};

//clear profiles
export const clearCurrentProfiles = () => {
  return {
    type: CLEAR_CURRENT_PROFILES
  };
};

//clear profiles
export const clearCurrentProfileOther = () => {
  return {
    type: CLEAR_CURRENT_PROFILE_OTHER
  };
};

export const setProfileLoadingOther = () => {
  return {
    type: PROFILE_LOADING_OTHER
  };
};

export const setProfileStopLoadingOther = () => {
  return {
    type: PROFILE_LOADING_FALSE_OTHER
  };
};

export const setProfileLoadingOther2 = () => {
  return {
    type: PROFILE_LOADING_2_OTHER
  };
};

export const setProfileStopLoadingOther2 = () => {
  return {
    type: PROFILE_LOADING_FALSE_2_OTHER
  };
};
