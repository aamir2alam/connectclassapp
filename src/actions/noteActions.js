import axios from "axios";
import {
  GET_NOTE,
  GET_NOTES,
  FILTER_NOTES,
  NOTES_LOADING,
  NOTES_LOADING_2,
  NOTES_LOADING_FALSE,
  NOTES_LOADING_2_FALSE,
  GET_ERRORS,
  CLEAR_CURRENT_NOTES,
  GET_REFRESHED_NOTES,
  GET_NOTICES,
  GET_REFRESHED_NOTICE,
  FILTER_NOTICE,
  GET_SAVED_ITEMS,
  FILTER_SAVED_ITEMS,
  CLEAR_SAVED_ITEMS,
  GET_RECENT_SAVED_ITEMS
} from "./types";
import { baseurl } from "../utils/baseUrl";

export const getStudentNotes = pageno => dispatch => {
  dispatch(setNotesLoading());
  console.log("getting all notes.. ", pageno);
  axios
    .get(`${baseurl}/api/notes/class/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno !== 1 ? GET_NOTES : GET_REFRESHED_NOTES,
        notes: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getTeacherNotes = pageno => dispatch => {
  dispatch(setNotesLoading());
  console.log("getting teacher notes.. ", pageno);
  axios
    .get(`${baseurl}/api/notes/teacher/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno !== 1 ? GET_NOTES : GET_REFRESHED_NOTES,
        notes: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getTeacherPersonalNotes = pageno => dispatch => {
  dispatch(setNotesLoading());
  axios
    .get(`${baseurl}/api/notes/teacher/personal/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno !== 1 ? GET_NOTES : GET_REFRESHED_NOTES,
        notes: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getSuggestionNotesStudent = (pageno, config) => dispatch => {
  dispatch(setNotesLoading());
  axios
    .get(`${baseurl}/api/notes/suggestions/${pageno}`, config)
    .then(res => {
      dispatch({
        type: pageno !== 1 ? GET_NOTES : GET_REFRESHED_NOTES,
        notes: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getStudentNotices = pageno => dispatch => {
  dispatch(setNotesLoading());
  console.log("getting notices " + pageno);
  axios
    .get(`${baseurl}/api/notice/student/${pageno}`)
    .then(res => {
      console.log(res.data);
      dispatch({
        type: pageno !== 1 ? GET_NOTICES : GET_REFRESHED_NOTICE,
        notices: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getNoticesForTeacher = pageno => dispatch => {
  dispatch(setNotesLoading());
  axios
    .get(`${baseurl}/api/notice/teacher/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno !== 1 ? GET_NOTICES : GET_REFRESHED_NOTICE,
        notices: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getSavedNotes = pageno => dispatch => {
  dispatch(setNotesLoading());
  console.log("getting saved ", pageno);
  axios
    .get(`${baseurl}/api/notes/saved/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno === 1 ? GET_RECENT_SAVED_ITEMS : GET_SAVED_ITEMS,
        saved_items: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getSavedNotices = pageno => dispatch => {
  dispatch(setNotesLoading());
  console.log("getting saved notices.. ", pageno);
  axios
    .get(`${baseurl}/api/notice/saved/${pageno}`)
    .then(res => {
      dispatch({
        type: pageno === 1 ? GET_RECENT_SAVED_ITEMS : GET_SAVED_ITEMS,
        saved_items: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const postNotestoClass = notesData => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${baseurl}/api/notes/class`, notesData)
      .then(res => {
        dispatch({
          type: GET_NOTE,
          payload: res.data
        });
        resolve("Promise Complete");
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        });
        reject(err);
      });
  });
};

export const postNotestoClassTeacher = notesData => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${baseurl}/api/notes/teacher`, notesData)
      .then(res => {
        dispatch({
          type: GET_NOTE,
          payload: res.data
        });
        resolve("Promise Complete");
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        });
        reject(err);
      });
  });
};

export const postNoticetoClass = noticeData => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${baseurl}/api/notice/students`, noticeData)
      .then(res => {
        dispatch({
          type: GET_NOTE,
          payload: res.data
        });
        resolve("Promise Complete");
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        });
        reject(err);
      });
  });
};

export const postNoticetoClassTeacher = noticeData => dispatch => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${baseurl}/api/notice/teacher`, noticeData)
      .then(res => {
        dispatch({
          type: GET_NOTE,
          payload: res.data
        });
        resolve("Promise Complete");
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        });
        reject(err);
      });
  });
};

export const bookmarkNotes = note_id => dispatch => {
  dispatch(setNotesLoading2());
  axios
    .post(`${baseurl}/api/notes/save/${note_id}`)
    .then(res => {
      dispatch({
        type: FILTER_NOTES,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const FilterBookmarkedItems = item => dispatch => {
  dispatch({
    type: FILTER_SAVED_ITEMS,
    payload: item
  });
};

export const bookmarkNotice = note_id => dispatch => {
  dispatch(setNotesLoading2());
  axios
    .post(`${baseurl}/api/notice/save/${note_id}`)
    .then(res => {
      console.log(res);
      dispatch({
        type: FILTER_NOTICE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const clearBookmarkedItems = () => {
  return {
    type: CLEAR_SAVED_ITEMS
  };
};
//clear current notes in redux state
export const clearCurrentNotes = () => {
  return {
    type: CLEAR_CURRENT_NOTES
  };
};

export const setNotesLoading = () => dispatch => {
  dispatch({
    type: NOTES_LOADING
  });
};

export const setStopNotesLoading = () => dispatch => {
  dispatch({
    type: NOTES_LOADING_FALSE
  });
};

export const setNotesLoading2 = () => dispatch => {
  dispatch({
    type: NOTES_LOADING_2
  });
};

export const setStopNotesLoading2 = () => dispatch => {
  dispatch({
    type: NOTES_LOADING_2_FALSE
  });
};
