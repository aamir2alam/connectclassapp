import axios from "axios";
import {
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_COUNT,
  NOTIFICATION_LOADING,
  NOTIFICATION_LOADING_FALSE,
  NOTIFICATION_LOADING2,
  NOTIFICATION_LOADING2_FALSE,
  GET_ERRORS,
  GET_REFRESHED_NOTIFICATIONS,
  CLEAR_NOTIFICATION_COUNT,
  UPDATE_NOTIFICATION
} from "./types";
import { baseurl } from "../utils/baseUrl";

export const getNotifications = pageno => dispatch => {
  dispatch(setNotificationLoading());
  axios
    .get(`${baseurl}/api/notifications/${pageno}`)
    .then(res => {
      console.log("getNotifications()  ", pageno);
      dispatch({
        type: pageno === 1 ? GET_REFRESHED_NOTIFICATIONS : GET_NOTIFICATIONS,
        notifications: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: GET_NOTIFICATIONS,
        notifications: []
      });
    });
};

export const getNewNotifications = pageno => dispatch => {
  console.log("searching for page ", pageno);

  return new Promise((resolve, reject) => {
    axios
      .get(`${baseurl}/api/notifications/new/notification/${pageno}`)
      .then(res => {
        dispatch({
          type: GET_REFRESHED_NOTIFICATIONS,
          notifications: res.data
        });
        resolve(res.data);
      })
      .catch(err => {
        dispatch({
          type: GET_NOTIFICATIONS,
          notifications: []
        });
        reject(err);
      });
  });
};

export const getNotificationsCount = () => dispatch => {
  console.log("getting count ");
  axios
    .get(`${baseurl}/api/notifications/count/new_notifications`)
    .then(res => {
      console.log(res);
      dispatch({
        type: GET_NOTIFICATIONS_COUNT,
        payload: res.data.count
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: GET_NOTIFICATIONS_COUNT,
        payload: 0
      });
    });
};

export const clearNotificationCount = () => dispatch => {
  dispatch({
    type: CLEAR_NOTIFICATION_COUNT
  });
};

export const updateNotification = notificationObj => dispatch => {
  dispatch({
    type: UPDATE_NOTIFICATION,
    payload: notificationObj
  });
};

export const setNotificationLoading = () => dispatch => {
  dispatch({
    type: NOTIFICATION_LOADING
  });
};

export const setStopNotificationLoading = () => dispatch => {
  dispatch({
    type: NOTIFICATION_LOADING_FALSE
  });
};

export const setNotificationLoading2 = () => dispatch => {
  dispatch({
    type: NOTIFICATION_LOADING2
  });
};

export const setStopNotificationLoading2 = () => dispatch => {
  dispatch({
    type: NOTIFICATION_LOADING2_FALSE
  });
};
