import { CHANGE_SWIPE_TAB_INDEX } from "../types";

export const changeSwipeTab = index => dispatch => {
  dispatch({
    type: CHANGE_SWIPE_TAB_INDEX,
    payload: index
  });
};
