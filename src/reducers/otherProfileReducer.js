import {
  GET_PROFILE_OTHER,
  PROFILE_LOADING_OTHER,
  CLEAR_CURRENT_PROFILE_OTHER,
  CLEAR_CURRENT_PROFILES,
  PROFILE_LOADING_FALSE_OTHER,
  GET_PROFILES,
  PROFILE_LOADING_FALSE_2_OTHER,
  PROFILE_LOADING_2_OTHER,
  FIND_PROFILES_LOADING,
  FIND_PROFILES_LOADING_FALSE
} from "../actions/types";

const initalState = {
  other_profile: null,
  other_profiles: null,
  other_loading: false,
  other_loading2: false
};

export default function(state = initalState, action) {
  switch (action.type) {
    case PROFILE_LOADING_OTHER:
      return {
        ...state,
        loading: true
      };
    case PROFILE_LOADING_FALSE_OTHER:
      return {
        ...state,
        loading: false
      };
    case PROFILE_LOADING_2_OTHER:
      return {
        ...state,
        loading2: true
      };
    case FIND_PROFILES_LOADING:
      return {
        ...state,
        loading: true
      };
    case FIND_PROFILES_LOADING_FALSE:
      return {
        ...state,
        loading: false
      };
    case PROFILE_LOADING_FALSE_2_OTHER:
      return {
        ...state,
        loading2: false
      };
    case GET_PROFILE_OTHER:
      return {
        ...state,
        profile: action.payload,
        loading: false
      };
    case GET_PROFILES:
      return {
        ...state,
        profiles: action.payload,
        loading2: false
      };
    case CLEAR_CURRENT_PROFILE_OTHER:
      return {
        ...state,
        profile: null
      };
    case CLEAR_CURRENT_PROFILES:
      return {
        ...state,
        profiles: null
      };
    default:
      return state;
  }
}
