import { SET_CURRENT_USER, LOGIN_LOADING, LOGIN_LOADING_FALSE } from "../actions/types";
import isEmpty from "../validation/is-empty";

const initalState = {
  isAuthenticated: false,
  user: {},
  loading: false
};

export default (state = initalState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return { ...state, isAuthenticated: !isEmpty(action.payload), user: action.payload, loading: false };
    case LOGIN_LOADING:
      return { ...state, loading: true };
    case LOGIN_LOADING_FALSE:
      return { ...state, loading: false };
    default:
      return state;
  }
};
