import {
  GET_NOTE,
  GET_NOTES,
  FILTER_NOTES,
  NOTES_LOADING,
  NOTES_LOADING_2,
  NOTES_LOADING_FALSE,
  NOTES_LOADING_2_FALSE,
  CLEAR_CURRENT_NOTES,
  GET_REFRESHED_NOTES,
  GET_NOTICES,
  GET_NOTICE,
  FILTER_NOTICE,
  GET_REFRESHED_NOTICE,
  GET_SAVED_ITEMS,
  GET_SAVED_ITEM,
  FILTER_SAVED_ITEMS,
  CLEAR_SAVED_ITEMS,
  GET_RECENT_SAVED_ITEMS
} from "../actions/types";
import isEmpty from "../validation/is-empty";
import __ from "lodash";

const initalState = {
  note: null,
  notice: null,
  notes: [],
  notices: [],
  saved_items: [],
  saved_item: null,
  hasMoreData: true,
  hasMoreData2: true, //for notice
  hasMoreData3: true, // for saved
  //notes=>loading, note => loading2
  loading: false,
  loading2: false
};

export default function(state = initalState, action) {
  const { notes = [] } = action;
  const { notices = [] } = action;
  const { saved_items = [] } = action;
  switch (action.type) {
    case NOTES_LOADING:
      return {
        ...state,
        loading: true
      };
    case NOTES_LOADING_FALSE:
      return {
        ...state,
        loading: false
      };
    case NOTES_LOADING_2:
      return {
        ...state,
        loading2: true
      };
    case NOTES_LOADING_2_FALSE:
      return {
        ...state,
        loading2: false
      };
    case GET_NOTES:
      if (notes.length < 10) {
        state.hasMoreData = false;
      } else {
        state.hasMoreData = true;
      }
      return {
        ...state,
        notes: [...state.notes, ...notes],
        loading: false
      };
    case GET_REFRESHED_NOTES:
      function comparer(otherArray) {
        return function(current) {
          return (
            otherArray.filter(function(other) {
              return other._id == current._id;
            }).length == 0
          );
        };
      }

      let onlyFreshNotes;

      if (!isEmpty(state.notes) && state.notes.length > 10) {
        onlyFreshNotes = notes.filter(comparer(state.notes.slice(0, 10)));
      } else if (!isEmpty(state.notes)) {
        onlyFreshNotes = notes.filter(comparer(state.notes));
      } else {
        onlyFreshNotes = notes;
      }
      if (isEmpty(state.notes) && onlyFreshNotes.length < 10) {
        state.hasMoreData = false;
      }
      // if(isEmpty(state.notes) && onlyFreshNotes.length < 10){
      //     state.hasMoreData = false;
      // }else{
      //     state.hasMoreData = true;
      // }
      return {
        ...state,
        notes: [...onlyFreshNotes, ...state.notes],
        loading: false
      };
    case GET_NOTE:
      return {
        ...state,
        note: action.payload,
        loading2: false
      };
    case FILTER_NOTES:
      return {
        ...state,
        loading2: false,
        notes: state.notes.map(note => {
          if (note._id === action.payload._id) {
            return action.payload;
          }
          return note;
        })
      };
    case CLEAR_CURRENT_NOTES:
      state.loading = false;
      state.hasMoreData = true;
      state.notes = [];
      return {
        ...state,
        notes: []
      };
    case GET_NOTICES:
      if (notices.length < 10) {
        state.hasMoreData2 = false;
      } else {
        state.hasMoreData2 = true;
      }
      return {
        ...state,
        notices: [...state.notices, ...notices],
        loading: false
      };
    case GET_REFRESHED_NOTICE:
      function comparer(otherArray) {
        return function(current) {
          return (
            otherArray.filter(function(other) {
              return other._id == current._id;
            }).length == 0
          );
        };
      }

      let onlyFreshNotices;

      if (!isEmpty(state.notices) && state.notices.length > 10) {
        onlyFreshNotices = notices.filter(comparer(state.notices.slice(0, 10)));
      } else if (!isEmpty(state.notices)) {
        onlyFreshNotices = notices.filter(comparer(state.notices));
      } else {
        onlyFreshNotices = notices;
      }
      if (isEmpty(state.notices) && onlyFreshNotices.length < 10) {
        state.hasMoreData2 = false;
      }
      // if(isEmpty(state.notes) && onlyFreshNotices.length < 10){
      //     state.hasMoreData = false;
      // }else{
      //     state.hasMoreData = true;
      // }
      return {
        ...state,
        notices: [...onlyFreshNotices, ...state.notices],
        loading: false
      };
    case GET_NOTICE:
      return {
        ...state,
        notice: action.payload,
        loading2: false
      };
    case FILTER_NOTICE:
      return {
        ...state,
        loading2: false,
        notices: state.notices.map(notice => {
          if (notice._id === action.payload._id) {
            return action.payload;
          }
          return notice;
        })
      };
    // for saved item states
    case GET_SAVED_ITEMS:
      if (saved_items.length < 10) {
        state.hasMoreData3 = false;
      } else {
        state.hasMoreData3 = true;
      }
      return {
        ...state,
        saved_items: [...state.saved_items, ...saved_items],
        loading: false
      };
    case GET_RECENT_SAVED_ITEMS:
      function comparer(otherArray) {
        return function(current) {
          return (
            otherArray.filter(function(other) {
              return other._id == current._id;
            }).length == 0
          );
        };
      }

      let onlyFreshItems;

      if (!isEmpty(state.saved_items) && state.saved_items.length > 10) {
        onlyFreshItems = saved_items.filter(comparer(state.saved_items.slice(0, 10)));
      } else if (!isEmpty(state.saved_items)) {
        onlyFreshItems = saved_items.filter(comparer(state.saved_items));
      } else {
        onlyFreshItems = saved_items;
      }
      if (isEmpty(state.saved_items) && onlyFreshItems.length < 10) {
        state.hasMoreData3 = false;
      }
      // if(isEmpty(state.notes) && onlyFreshItems.length < 10){
      //     state.hasMoreData = false;
      // }else{
      //     state.hasMoreData = true;
      // }
      return {
        ...state,
        saved_items: [...onlyFreshItems, ...state.saved_items],
        loading: false
      };
    case GET_SAVED_ITEM:
      return {
        ...state,
        saved_item: action.payload,
        loading2: false
      };
    case FILTER_SAVED_ITEMS:
      return {
        ...state,
        loading2: false,
        // saved_items: state.saved_items
        saved_items: __.remove(state.saved_items, item => {
          return item._id !== action.payload._id;
        })
      };
    case CLEAR_SAVED_ITEMS:
      state.loading = false;
      state.hasMoreData3 = true;
      state.saved_items = [];
      return {
        ...state,
        saved_items: []
      };
    default:
      return state;
  }
}
