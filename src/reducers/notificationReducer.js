import {
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_COUNT,
  NOTIFICATION_LOADING,
  NOTIFICATION_LOADING_FALSE,
  NOTIFICATION_LOADING2,
  NOTIFICATION_LOADING2_FALSE,
  GET_REFRESHED_NOTIFICATIONS,
  CLEAR_NOTIFICATION_COUNT,
  UPDATE_NOTIFICATION
} from "../actions/types";
import isEmpty from "../validation/is-empty";

const initalState = {
  notifications: [],
  hasMoreData: true,
  count: 0,
  loading: false,
  loading2: false
};

export default function(state = initalState, action) {
  const { notifications = [] } = action;
  switch (action.type) {
    case NOTIFICATION_LOADING:
      return {
        ...state,
        loading: true
      };
    case NOTIFICATION_LOADING_FALSE:
      return {
        ...state,
        loading: false
      };
    case NOTIFICATION_LOADING2:
      return {
        ...state,
        loading2: true
      };
    case NOTIFICATION_LOADING2_FALSE:
      return {
        ...state,
        loading2: false
      };
    case GET_NOTIFICATIONS:
      if (notifications.length < 10) {
        state.hasMoreData = false;
      } else {
        state.hasMoreData = true;
      }
      return {
        ...state,
        notifications: [...state.notifications, ...notifications],
        loading: false
      };
    case GET_REFRESHED_NOTIFICATIONS:
      function comparer(otherArray) {
        return function(current) {
          return (
            otherArray.filter(function(other) {
              return other._id == current._id;
            }).length == 0
          );
        };
      }

      let onlyFreshNotif;

      if (!isEmpty(state.notifications) && state.notifications.length > 10) {
        onlyFreshNotif = notifications.filter(comparer(state.notifications.slice(0, 10)));
      } else if (!isEmpty(state.notifications)) {
        onlyFreshNotif = notifications.filter(comparer(state.notifications));
      } else {
        onlyFreshNotif = notifications;
      }
      // if (isEmpty(state.notifications) && onlyFreshNotif.length < 10) {
      //   state.hasMoreData = false;
      // }
      // if(isEmpty(state.notifications) && onlyFreshNotif.length < 10){
      //     state.hasMoreData = false;
      // }else{
      //     state.hasMoreData = true;
      // }
      return {
        ...state,
        notifications: [...onlyFreshNotif, ...state.notifications],
        loading: false
      };
    case UPDATE_NOTIFICATION:
      return {
        ...state,
        notifications: state.notifications.map(notif => {
          if (notif._id === action.payload._id) {
            notif.seen = true;
            return notif;
          }
          return notif;
        })
      };
    case GET_NOTIFICATIONS_COUNT:
      return {
        ...state,
        count: action.payload > 0 ? action.payload : state.count
      };
    case CLEAR_NOTIFICATION_COUNT:
      return {
        ...state,
        count: 0
      };
    default:
      return state;
  }
}
