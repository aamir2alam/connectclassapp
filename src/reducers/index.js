import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import notificationReducer from "./notificationReducer";
import notesReducer from "./notesReducer";
import profileReducer from "./profileReducer";
import otherProfileReducer from "./otherProfileReducer";
import navigationReducer from "./inAppReducer/navigationReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  notification: notificationReducer,
  notes: notesReducer,
  profile: profileReducer,
  other_profile: otherProfileReducer,
  navigationState: navigationReducer
});
