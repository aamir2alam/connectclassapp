import {
  GET_PROFILE,
  PROFILE_LOADING,
  PROFILE_LOADING_2,
  CLEAR_CURRENT_PROFILE,
  PROFILE_LOADING_FALSE,
  PROFILE_LOADING_FALSE_2,
  GET_UPLOAD_PROGRESS
} from "../actions/types";

const initalState = {
  profile: null,
  loading: false,
  loading2: false,
  progress: 0
};

export default function(state = initalState, action) {
  switch (action.type) {
    case PROFILE_LOADING:
      return {
        ...state,
        loading: true
      };
    case PROFILE_LOADING_FALSE:
      return {
        ...state,
        loading: false
      };
    case PROFILE_LOADING_2:
      return {
        ...state,
        loading2: true
      };
    case PROFILE_LOADING_FALSE_2:
      return {
        ...state,
        loading2: false
      };
    case GET_PROFILE:
      return {
        ...state,
        profile: action.payload,
        loading: false,
        progress: 0
      };
    case GET_UPLOAD_PROGRESS:
      return {
        ...state,
        progress: action.payload
      };
    case CLEAR_CURRENT_PROFILE:
      return {
        ...state,
        profile: null
      };
    default:
      return state;
  }
}
