import { CHANGE_SWIPE_TAB_INDEX } from "../../actions/types";

const initalState = {
  tabIndex: 0,
  currentPage: ""
};

export default (state = initalState, action) => {
  switch (action.type) {
    case CHANGE_SWIPE_TAB_INDEX:
      return { ...state, tabIndex: action.payload };
    default:
      return state;
  }
};
