import React, { Component } from "react";

import { Provider } from "react-redux";
import { Root } from "native-base";

import store from "./store";

import AuthNavigation from "./Routes/AuthNavigation";

const App = () => {
  //const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
      <Root>
        <AuthNavigation />
      </Root>
    </Provider>
  );
};

export default App;
