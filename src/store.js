import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
//import { composeWithDevTools } from "remote-redux-devtool";

import rootReducer from "./reducers";

const intialState = {};

const middleware = [thunk];

// const composeEnhancers =
//   typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
//         // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
//       })
//     : compose;

// let debuggWrapper = data => data;
// if (__DEV__) {
//   const { composeWithDevTools } = require("remote-redux-devtool");
//   debuggWrapper = composeWithDevTools({ realtime: true, port: 8000 });
// }
// const store = createStore(
//   rootReducer,
//   intialState,
//   debuggWrapper(
//     compose(
//       applyMiddleware(...middlewares),
//       enhancer
//     )
//   )
// );

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware)
  // other store enhancers if any
);
/**
 * First argument reducers
 * Second argument initial state to the application
 * Thirst argument setup middle wares using redux thunk
 */
const store = createStore(rootReducer, intialState, enhancer);

export default store;
