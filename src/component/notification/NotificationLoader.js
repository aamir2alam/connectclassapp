import React from "react";
import ContentLoader from "rn-content-loader/src";
import { Circle, Rect } from "react-native-svg";
import { View } from "react-native";

const NotificationLoader = () => {
  return (
    <View style={{ alignItems: "center", marginTop: 5 }}>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Circle cx="20" cy="20" r="20" />
        <Rect x="75" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
    </View>
  );
};

export { NotificationLoader };
