import React, { PureComponent } from "react";
import { Text, FlatList, RefreshControl, View, Platform, AppState, TouchableNativeFeedback } from "react-native";
import { connect } from "react-redux";
import {
  getNotifications,
  getNewNotifications,
  clearNotificationCount,
  updateNotification
} from "../../actions/notificationActions";
import { getProfileByHandle, getProfileById } from "../../actions/otherProfileActions";
import isEmpty from "../../validation/is-empty";
import { Header, Left, Body, Right, Button, Title, Thumbnail, ListItem, List, Spinner } from "native-base";
import PushController from "./PushController";
import { DrawerActions } from "react-navigation";
import TimeAgo from "react-native-timeago";
import commonColor from "../../../native-base-theme/variables/commonColor";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { changeSwipeTab } from "../../actions/inAppActions/navigationActions";

class Notifications extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
      loading: false,
      page: 1,
      page_push_notif: 1,
      refreshing: false,
      moreLoading: false,
      hasMoreData: false
    };
  }
  componentDidMount = () => {
    this._isMounted = true;

    this.props.navigation.addListener("willFocus", route => {
      this.props.getNotifications(1);
      this.setState({
        refreshing: true
      });
      if (this.props.count > 0) {
        this.props.clearNotificationCount();
      }
    });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }
  componentWillUpdate() {
    if (this.props.navigation.state.routeName === "NotificationPage" && this.props.count > 0) {
      this.props.clearNotificationCount();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps) && nextProps.notifications !== this.props.notifications && this._isMounted) {
      this.setState({
        refreshing: false,
        moreLoading: false,
        loading: false
      });

      if (!nextProps.hasMoreData && this._isMounted) {
        this.setState({
          hasMoreData: false,
          refreshing: false
        });
      }
    } else {
      if (this._isMounted)
        this.setState({
          refreshing: false,
          loading: false
        });
    }
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true
    });
    this.props.getNotifications(1);
  };

  handleLoadMoreData = () => {
    if (!this.props.hasMoreData) {
      return;
    }
    this.setState({
      page: this.state.page + 1,
      moreLoading: true
    });
    this.props.getNotifications(this.state.page + 1);
  };

  handleNotificationClick = notif => {
    if (!notif.seen) this.props.updateNotification(notif);

    switch (notif.reference.resource_type) {
      case "notice":
        this.props.navigation.navigate("Home");
        this.props.changeSwipeTab(1);
        break;
      case "notes":
        this.props.navigation.navigate("Home");
        this.props.changeSwipeTab(0);
        break;
      case "profile":
        this.props.navigation.navigate("OtherProfile");
        if (!isEmpty(notif.from_user.profile_id.handle)) {
          this.props.getProfileByHandle(notif.from_user.profile_id.handle);
        } else {
          this.props.getProfileById(notif.from_user.profile_id._id);
        }
        break;
      default:
        return;
    }
  };

  onBars() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  /**
   *
   * @param {*notification object} param
   * @description showing only notification for profile, notes and notices
   */
  renderItem(notification) {
    if (isEmpty(notification)) {
      return (
        <View>
          <Text>No notifications</Text>
        </View>
      );
    }

    if (notification.item.reference.resource_type === "post") {
      return;
    }

    const myNotificationFunction = param => {
      switch (param.item.reference.resource_type) {
        // case "post":
        //   return param.item.event_type === "like" ? (
        //     <Text>
        //       {"" +
        //         param.item.from_user.user_id.name +
        //         " " +
        //         param.item.event_type +
        //         "d your" +
        //         " " +
        //         param.item.reference.resource_type +
        //         "."}
        //     </Text>
        //   ) : (
        //     <Text>
        //       {"" +
        //         param.item.from_user.user_id.name +
        //         " " +
        //         param.item.event_type +
        //         "ed on your" +
        //         " " +
        //         param.item.reference.resource_type +
        //         "."}
        //     </Text>
        //   );
        case "profile":
          return (
            <Text style={{ color: "#212121" }}>
              {param.item.from_user.user_id.name + " " + param.item.event_type + "ed you."}
            </Text>
          );
        case "notice":
          return (
            <Text style={{ color: "#212121" }}>
              {param.item.from_user.user_id.name +
                " " +
                param.item.event_type +
                "ed a new " +
                param.item.reference.resource_type +
                " in your classroom."}
            </Text>
          );
        case "notes":
          return (
            <Text style={{ color: "#212121" }}>
              {param.item.from_user.user_id.name +
                " " +
                param.item.event_type +
                "ed a new " +
                param.item.reference.resource_type +
                " in your classroom."}
            </Text>
          );
      }
    };

    return (
      <List style={!notification.item.seen ? { backgroundColor: "#ECEFF1", width: "100%" } : {}}>
        <ListItem avatar>
          <TouchableNativeFeedback
            onPress={() => {
              this.props.navigation.navigate("OtherProfile");
              if (!isEmpty(notification.item.from_user.profile_id.handle)) {
                this.props.getProfileByHandle(notification.item.from_user.profile_id.handle);
              } else {
                this.props.getProfileById(notification.item.from_user.profile_id._id);
              }
            }}
          >
            <Left>
              <Thumbnail
                source={{
                  uri: notification.item.from_user.profile_id.profile_picture
                }}
                style={{ marginTop: 5, marginRight: 2, height: 36, width: 36 }}
              />
            </Left>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback onPress={() => this.handleNotificationClick(notification.item)}>
            <Body>
              <Text>{myNotificationFunction(notification)}</Text>
              <TimeAgo time={notification.item.date} style={{ paddingTop: 5, fontSize: 12 }} />
            </Body>
          </TouchableNativeFeedback>
        </ListItem>
      </List>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header style={{ backgroundColor: "#f9f9f9" }}>
          <Left>
            <Button transparent onPress={this.onBars.bind(this)}>
              <Icon name="menu" style={{ color: commonColor.blue800 }} size={20} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "#212121", alignSelf: "center" }}>Notifications</Title>
          </Body>
          <Right />
        </Header>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.props.notifications}
            extraData={this.props.loading2}
            renderItem={this.renderItem.bind(this)}
            keyExtractor={(item, index) => item._id}
            onEndReached={this.handleLoadMoreData}
            onEndReachedThreshold={0.2}
            initialNumToRender={8}
            refreshControl={
              <RefreshControl colors={["#0277BD"]} refreshing={this.state.refreshing} onRefresh={this.handleRefresh} />
            }
            ListHeaderComponent={
              isEmpty(this.props.notifications) && !this.props.loading ? (
                <Text style={{ alignSelf: "center", marginTop: 20 }}>No new notifications</Text>
              ) : (
                <View />
              )
            }
            ListFooterComponent={this.props.loading ? <Spinner color={commonColor.blue800} /> : <View />}
          />
          <PushController navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    notifications: state.notification.notifications,
    hasMoreData: state.notification.hasMoreData,
    loading: state.notification.loading,
    loading2: state.notification.loading2,
    errors: state.errors,
    count: state.notification.count
  };
};

export default connect(
  mapStateToProps,
  {
    getNotifications,
    getNewNotifications,
    getProfileByHandle,
    getProfileById,
    clearNotificationCount,
    updateNotification,
    changeSwipeTab
  }
)(Notifications);
