import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { View, RefreshControl, FlatList, Alert } from "react-native";
import {
  getStudentNotices,
  getNoticesForTeacher,
  bookmarkNotice,
  getStudentNotes,
  clearCurrentNotes
} from "../../actions/noteActions";
import { getProfileByHandle } from "../../actions/otherProfileActions";
import RNFetchBlob from "react-native-fetch-blob";
import { PermissionsAndroid } from "react-native";
import isEmpty from "../../validation/is-empty";
import { NoticeCard } from "../common/NoticeCard";
import { Spinner, Text } from "native-base";
import commonColor from "../../../native-base-theme/variables/commonColor";
import { NotesLoader } from "../common/NotesLoader";

class NoticePage extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      pageno: 1,
      refreshing: false,
      loading: false,
      loading2: false,
      loading_id: "",
      hasMoreData: true,
      moreLoading: false,
      isOpen: false,
      isDisabled: false,
      showToast: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.props.getStudentNotices(this.state.pageno);
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps) && nextProps.notes !== this.props.notes && this._isMounted) {
      this.setState({
        refreshing: false,
        moreLoading: false,
        loading: false
      });

      if (!nextProps.hasMoreData && this._isMounted) {
        this.setState({
          hasMoreData: false
        });
      }
    } else {
      if (this._isMounted)
        this.setState({
          refreshing: false,
          loading: false
        });
    }
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true
    });
    this.props.getStudentNotices(1);
  };

  handleLoadMoreData = () => {
    if (!this.props.hasMoreData) {
      return;
    }
    this.setState({
      pageno: this.state.pageno + 1,
      moreLoading: true
    });
    this.props.getStudentNotices(this.state.pageno + 1);
  };

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        title: "Storage Permission",
        message: "App needs access to your storage",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // permission granted
      } else {
        // permission granted
      }
    } catch (err) {
      console.warn(err);
    }
  }

  handleDownload = data => {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      .then(permission => {
        if (permission == true) {
          const { config, fs } = RNFetchBlob;
          let PictureDir = fs.dirs.PictureDir; // this is the pictures directory.
          var date = new Date();
          let options = {
            fileCache: true,
            addAndroidDownloads: {
              useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
              notification: false,
              path: PictureDir + "/note_" + data.filename, // TODO: add original filename
              description: "Downloading file..."
            }
          };
          config(options)
            .fetch("GET", data.filename)
            .then(res => {});
        } else {
          this.requestStoragePermission();
        }
      })
      .catch(err => {
        Alert("Failed to download file");
      });
  };

  handleBookMark = item => {
    this.setState(
      {
        loading_id: item._id
      },
      () => {
        this.props.bookmarkNotice(item._id);
      }
    );
  };

  openProfile = handle => {
    if (isEmpty(handle)) {
      alert("Username not found");
      return;
    }
    this.props.navigation.navigate("OtherProfile");
    this.props.getProfileByHandle(handle);
  };

  renderItem(item) {
    return (
      <NoticeCard
        style={{ elevation: 9, padding: 0 }}
        data={item}
        onBookmark={() => this.handleBookMark(item.item)}
        onProfileByHandle={() => this.openProfile(item.item.uploaded_by.profile_id.handle)}
        onDownload={() => this.handleDownload(item.item)}
        loading_id={this.state.loading_id}
        loading2={this.props.loading2}
      />
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {this.props.loading && !this.state.moreLoading ? (
            <NotesLoader />
          ) : (
            <FlatList
              data={this.props.notes}
              extraData={this.props.loading2}
              renderItem={this.renderItem.bind(this)}
              keyExtractor={(item, index) => item._id}
              initialNumToRender={7}
              refreshControl={
                <RefreshControl
                  colors={["#0277BD"]}
                  onRefresh={this.handleRefresh}
                  refreshing={this.state.refreshing}
                />
              }
              onEndReached={this.handleLoadMoreData}
              onEndReachedThreshold={0.5}
              ListHeaderComponent={
                isEmpty(this.props.notes) && !this.props.loading ? (
                  <Text style={{ alignSelf: "center", marginTop: 20 }}>No notices available yet</Text>
                ) : (
                  <View />
                )
              }
              ListFooterComponent={this.props.loading ? <Spinner color={commonColor.blue800} /> : <View />}
            />
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    notes: state.notes.notices,
    note: state.notes.notice,
    loading: state.notes.loading,
    loading2: state.notes.loading2,
    hasMoreData: state.notes.hasMoreData2,
    auth: state.auth,
    errors: state.errors,
    tabIndex: state.navigationState.tabIndex
  };
};

export default connect(
  mapStateToProps,
  {
    getStudentNotices,
    getNoticesForTeacher,
    bookmarkNotice,
    getStudentNotes,
    clearCurrentNotes,
    getProfileByHandle
  }
)(NoticePage);
