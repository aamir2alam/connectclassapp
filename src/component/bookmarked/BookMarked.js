import React, { PureComponent } from "react";
import { View, ScrollView, RefreshControl, FlatList, Alert } from "react-native";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import {
  getSavedNotes,
  getSavedNotices,
  clearCurrentNotes,
  getStudentNotes,
  bookmarkNotes,
  bookmarkNotice,
  FilterBookmarkedItems,
  clearBookmarkedItems
} from "../../actions/noteActions";
import { getProfileByHandle } from "../../actions/otherProfileActions";
import isEmpty from "../../validation/is-empty";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Right,
  Button,
  Segment,
  Content,
  Text,
  ListItem,
  Spinner
} from "native-base";
import { NoticeCard } from "../common/NoticeCard";
import RNFetchBlob from "react-native-fetch-blob";
import { PermissionsAndroid } from "react-native";
import Modal from "react-native-modalbox";
import { styles } from "../homePage/styles";
import ContentLoader from "rn-content-loader";
import { Circle, Rect } from "react-native-svg";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import commonColor from "../../../native-base-theme/variables/commonColor";

class BookMarked extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      pageno: 1,
      currentPanel: "notes",
      refreshing: false,
      hasMoreData: true,
      moreLoading: false,
      drawerOpen: false,
      loading: false,
      loading2: false,
      loading_id: "",
      isDisabled: false
    };
    this.handleBackPress = this.handleBackPress.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.props.clearBookmarkedItems();
    this.props.getSavedNotes(this.state.pageno);
    // attach hardware backButtion event listener
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    this._isMounted = false;
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.saved_items !== this.props.saved_items && this._isMounted) {
      this.setState({
        refreshing: false,
        moreLoading: false,
        loading: false
      });
      if (!nextProps.hasMoreData) {
        this.setState({
          hasMoreData: false
        });
      }
    } else {
      if (this._isMounted)
        this.setState({
          refreshing: false,
          loading: false
        });
    }
  }

  openModal() {
    this.refs.modal.open();
  }

  onFilter(filter) {
    this.refs.modal.close();
    switch (filter) {
      case "notes":
        if (this.state.currentPanel !== "notes") {
          this.setState({
            refreshing: true,
            currentPanel: "notes",
            pageno: 1
          });
          this.props.clearBookmarkedItems();
          this.props.getSavedNotes(1);
        }
        break;
      case "notices":
        if (this.state.currentPanel !== "notices") {
          this.setState({
            refreshing: true,
            currentPanel: "notices",
            pageno: 1
          });
          this.props.clearBookmarkedItems();
          this.props.getSavedNotices(1);
        }
        break;
    }
  }

  handleRefresh = () => {
    switch (this.state.currentPanel) {
      case "notes":
        this.setState(
          {
            refreshing: true
          },
          () => {
            this.props.getSavedNotes(1);
          }
        );
        break;
      case "notices":
        this.setState(
          {
            refreshing: true
          },
          () => {
            this.props.getSavedNotices(1);
          }
        );
        break;
    }
  };

  handleLoadMoreData = () => {
    if (!this.props.hasMoreData) {
      return;
    }
    this.setState({
      pageno: this.state.pageno + 1,
      moreLoading: true
    });
    if (this.state.currentPanel === "notes") {
      this.props.getSavedNotes(this.state.pageno + 1);
    } else {
      this.props.getSavedNotices(this.state.pageno + 1);
    }
  };

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        title: "Storage Permission",
        message: "App needs access to your storage",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // permission granted
      } else {
        // permission denied
      }
    } catch (err) {
      alert("Permission error");
    }
  }

  handleDownload = data => {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      .then(permission => {
        if (permission == true) {
          const { config, fs } = RNFetchBlob;
          let PictureDir = fs.dirs.PictureDir; // this is the pictures directory.
          var date = new Date();
          let options = {
            fileCache: true,
            addAndroidDownloads: {
              useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
              notification: false,
              path: PictureDir + "/note_" + data.filename, // TODO: add original filename
              description: "Downloading file..."
            }
          };
          config(options)
            .fetch("GET", data.filename)
            .then(res => {});
        } else {
          this.requestStoragePermission();
        }
      })
      .catch(err => {
        Alert.alert("Failed", "Failed to download the file", [{ text: "OK" }]);
      });
  };

  handleBookMark = item => {
    Alert.alert(
      "Remove " + this.state.currentPanel + " ?",
      "Are you sure",
      [
        {
          text: "Cancel",
          // onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.setState(
              {
                loading_id: item._id
              },
              () => {
                if (this.state.currentPanel === "notices") {
                  this.props.bookmarkNotice(item._id);
                  this.props.FilterBookmarkedItems(item);
                } else {
                  this.props.bookmarkNotes(item._id);
                  this.props.FilterBookmarkedItems(item);
                }
              }
            );
          }
        }
      ],
      { cancelable: true }
    );
  };

  renderItem = item => {
    return (
      <NoticeCard
        style={{ elevation: 9 }}
        data={item}
        onBookmark={() => {
          this.handleBookMark(item.item);
        }}
        onProfileByHandle={() => {
          this.props.navigation.navigate("OtherProfile");
          if (!isEmpty(item.item.uploaded_by.profile_id.handle)) {
            this.props.getProfileByHandle(item.item.uploaded_by.profile_id.handle);
          } else {
            this.props.getProfileById(item.item.uploaded_by.profile_id._id);
          }
        }}
        onDownload={() => this.handleDownload(item.item)}
        loading_id={this.state.loading_id}
        loading2={this.props.loading2}
      />
    );
  };

  renderModal() {
    return (
      <Modal
        backdrop
        style={[styles.Modal, styles.modalWrapped]}
        position={"center"}
        ref={"modal"}
        animationDuration={400}
        isDisabled={this.state.isDisabled}
      >
        <Container style={{ alignSelf: "stretch" }}>
          <Header style={{ backgroundColor: "#f9f9f9", color: "#212121" }}>
            <Body>
              <Title style={{ color: "#212121" }}>Apply Filters</Title>
            </Body>
          </Header>
          <Content>
            <ListItem icon onPress={() => this.onFilter("notes")}>
              <Left>
                <Button style={{ backgroundColor: commonColor.blue800 }}>
                  <Icon active name="book" size={20} color="#ffffff" />
                </Button>
              </Left>
              <Body>
                <Text>Notes</Text>
              </Body>
              <Right />
            </ListItem>
            <ListItem icon onPress={() => this.onFilter("notices")}>
              <Left>
                <Button style={{ backgroundColor: commonColor.blue800 }}>
                  <Icon active name="clipboard-check-outline" size={20} color="#ffffff" />
                </Button>
              </Left>
              <Body>
                <Text>Notices</Text>
              </Body>
              <Right />
            </ListItem>
          </Content>
        </Container>
      </Modal>
    );
  }

  handleBack = () => {
    this.props.navigation.goBack();
  };

  handleBackPress = () => {
    //this.props.navigation.goBack();
    //  this.props.clearCurrentNotes();
    //  this.props.getStudentNotes(1);
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderModal()}
        <Header style={{ backgroundColor: "#f9f9f9", color: "#212121" }}>
          <Left>
            <Button transparent onPress={this.handleBack.bind(this)}>
              <Icon name="keyboard-backspace" size={20} style={{ color: commonColor.blue800 }} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "#212121", alignSelf: "center" }}>Saved {this.state.currentPanel}</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.openModal.bind(this)}>
              <Icon name="filter-variant" style={{ color: commonColor.blue800 }} size={20} />
            </Button>
          </Right>
        </Header>

        <View style={{ flex: 1 }}>
          {this.state.loading === true ? (
            <View>
              <ContentLoader height={300}>
                <Circle cx="30" cy="30" r="30" />
                <Rect x="75" y="13" rx="4" ry="4" width="100" height="13" />
                <Rect x="75" y="37" rx="4" ry="4" width="50" height="8" />
                <Rect x="0" y="70" rx="5" ry="5" width="400" height="200" />
              </ContentLoader>
            </View>
          ) : (
            <FlatList
              data={this.props.saved_items}
              extraData={this.props.loading2}
              renderItem={this.renderItem.bind(this)}
              keyExtractor={(item, index) => item._id}
              initialNumToRender={7}
              refreshControl={
                <RefreshControl
                  colors={["#0277BD"]}
                  onRefresh={this.handleRefresh}
                  refreshing={this.state.refreshing}
                />
              }
              onEndReached={this.handleLoadMoreData}
              onEndReachedThreshold={0.5}
              ListHeaderComponent={
                isEmpty(this.props.saved_items) && !this.props.loading ? (
                  <Text style={{ alignSelf: "center", marginTop: 20 }}>No saved {this.state.currentPanel}</Text>
                ) : (
                  <View />
                )
              }
              ListFooterComponent={this.props.loading ? <Spinner color={commonColor.blue800} /> : <View />}
            />
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    saved_items: state.notes.saved_items,
    loading: state.notes.loading,
    loading2: state.notes.loading2,
    hasMoreData: state.notes.hasMoreData3,
    auth: state.auth,
    errors: state.errors
  };
};

export default connect(
  mapStateToProps,
  {
    getSavedNotes,
    getSavedNotices,
    clearCurrentNotes,
    getStudentNotes,
    getProfileByHandle,
    bookmarkNotes,
    bookmarkNotice,
    FilterBookmarkedItems,
    clearBookmarkedItems
  }
)(BookMarked);
