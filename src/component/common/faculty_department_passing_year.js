export const options_faculty = [
  {
    label: "Select Faculty",
    value: ""
  },

  {
    label: "Arts",
    value: "Arts"
  },
  {
    label: "Commerce",
    value: "Commerce"
  },
  {
    label: "Engineering",
    value: "Engineering"
  },
  {
    label: "Life Sciences",
    value: "Life Sciences"
  },
  {
    label: "Law",
    value: "Law"
  },
  {
    label: "Medicine",
    value: "Medicine"
  },
  {
    label: "Management",
    value: "Management"
  },
  {
    label: "Science",
    value: "Science"
  },
  {
    label: "Social Science",
    value: "Social Science"
  },
  {
    label: "Theology",
    value: "Theology"
  },
  {
    label: "Unani",
    value: "Unani"
  }
];

export const options_department = faculty_name => {
  let departmentOption;
  if (faculty_name === "Engineering") {
    departmentOption = options_department_engineering;
  } else if (faculty_name === "Arts") {
    departmentOption = options_department_arts;
  } else if (faculty_name === "Commerce") {
    departmentOption = options_department_commerce;
  } else if (faculty_name === "Life Sciences") {
    departmentOption = options_department_life_sciences;
  } else if (faculty_name === "Law") {
    departmentOption = options_department_law;
  } else if (faculty_name === "Medicine") {
    departmentOption = options_department_medicine;
  } else if (faculty_name === "Management") {
    departmentOption = options_department_management;
  } else if (faculty_name === "Science") {
    departmentOption = options_department_science;
  } else if (faculty_name === "Social Science") {
    departmentOption = options_department_social_science;
  } else if (faculty_name === "Theology") {
    departmentOption = options_department_theology;
  } else if (faculty_name === "Unani") {
    departmentOption = options_department_unani;
  } else {
    departmentOption = [
      {
        label: "Select Department",
        value: ""
      },
      {
        label: "Select Faculty First",
        value: ""
      }
    ];
  }
  return departmentOption;
};

export const options_department_engineering = [
  {
    label: "Select Department",
    value: ""
  },

  {
    label: "Applied Physics",
    value: "Applied Physics"
  },
  {
    label: "Applied Chemistry",
    value: "Applied Chemistry"
  },
  {
    label: "Applied Mathematics",
    value: "Applied Mathematics"
  },
  {
    label: "Architecture",
    value: "Architecture"
  },
  {
    label: "Chemical Engineering",
    value: "Chemical Engineering"
  },
  {
    label: "Civil Engineering",
    value: "Civil Engineering"
  },
  {
    label: "Computer Engineering",
    value: "Computer Engineering"
  },
  {
    label: "Electrical Engineering",
    value: "Electrical Engineering"
  },
  {
    label: "Electronics Engineering",
    value: "Electronics Engineering"
  },
  {
    label: "Mechanical Engineering",
    value: "Mechanical Engineering"
  },
  {
    label: "Petroleum Studies",
    value: "Petroleum Studies"
  }
];

export const options_department_arts = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Arabic",
    value: "Arabic"
  },
  {
    label: "English",
    value: "English"
  },
  {
    label: "Fine Arts",
    value: "Fine Arts"
  },
  {
    label: "Hindi",
    value: "Hindi"
  },
  {
    label: "Linguistics",
    value: "Linguistics"
  },
  {
    label: "Modern Indian Languages",
    value: "Modern Indian Languages"
  },
  {
    label: "Persian",
    value: "Persian"
  },
  {
    label: "Philosophy",
    value: "Philosophy"
  },
  {
    label: "Sanskrit",
    value: "Sanskrit"
  },
  {
    label: "Urdu",
    value: "Urdu"
  }
];

export const options_department_commerce = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Commerce",
    value: "Commerce"
  }
];

export const options_department_life_sciences = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Biochemistry",
    value: "Biochemistry"
  },
  {
    label: "Botany",
    value: "Botany"
  },
  {
    label: "Museology",
    value: "Museology"
  },
  {
    label: "Wildlife Sciences",
    value: "Wildlife Sciences"
  },
  {
    label: "Zoology",
    value: "Zoology"
  }
];

export const options_department_law = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Law",
    value: "Law"
  }
];

export const options_department_medicine = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Anatomy",
    value: "Anatomy"
  },
  {
    label: "Anesthesiology",
    value: "Anesthesiology"
  },
  {
    label: "Biochemistry",
    value: "Biochemistry"
  },
  {
    label: "Cardiothoracic Surgery",
    value: "Cardiothoracic Surgery"
  },
  {
    label: "Community Medicine",
    value: "Community Medicine"
  },
  {
    label: "Dermatology",
    value: "Dermatology"
  },
  {
    label: "Ortho Laryngology (ENT)",
    value: "Ortho Laryngology (ENT)"
  },
  {
    label: "Forensic Medicine",
    value: "Forensic Medicine"
  },
  {
    label: "Medicine",
    value: "Medicine"
  },
  {
    label: "Microbiology",
    value: "Microbiology"
  },
  {
    label: "Neuro Surgery",
    value: "Neuro Surgery"
  },
  {
    label: "Obstetrics & Gynaecology",
    value: "Obstetrics & Gynaecology"
  },
  {
    label: "Orthopaedic Surgery",
    value: "Orthopaedic Surgery"
  },
  {
    label: "Paediatrics",
    value: "Paediatrics"
  },
  {
    label: "Paediatric Surgery",
    value: "Paediatric Surgery"
  },
  {
    label: "Pharmacology",
    value: "Pharmacology"
  },
  {
    label: "Physiology",
    value: "Physiology"
  },
  {
    label: "Pathology",
    value: "Pathology"
  },
  {
    label: "Plastic Surgery",
    value: "Plastic Surgery"
  },
  {
    label: "Psychiatary",
    value: "Psychiatary"
  },
  {
    label: "Radio Diagnosis",
    value: "Radio Diagnosis"
  },
  {
    label: "Radio Therapy",
    value: "Radio Therapy"
  },
  {
    label: "Surgery",
    value: "Surgery"
  },
  {
    label: "Surgery",
    value: "Surgery"
  },
  {
    label: "TB & Respiratory Diseases",
    value: "TB & Respiratory Diseases"
  }
];

export const options_department_management = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Business Administration",
    value: "Business Administration"
  }
];

export const options_department_science = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Chemistry",
    value: "Chemistry"
  },
  {
    label: "Computer Science",
    value: "Computer Science"
  },
  {
    label: "Geography",
    value: "Geography"
  },
  {
    label: "Geology",
    value: "Geology"
  },
  {
    label: "Mathematics",
    value: "Mathematics"
  },
  {
    label: "Physics",
    value: "Physics"
  },
  {
    label: "Statistics and Operations Research",
    value: "Statistics and Operations Research"
  },
  {
    label: "Remote Sensing and GIS Applications",
    value: "Remote Sensing and GIS Applications"
  }
];

export const options_department_social_science = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Economics",
    value: "Economics"
  },
  {
    label: "Education",
    value: "Education"
  },
  {
    label: "History",
    value: "History"
  },
  {
    label: "Islamic Studies",
    value: "Islamic Studies"
  },
  {
    label: "Library and Information Sciences",
    value: "Library and Information Sciences"
  },
  {
    label: "Mass Communication",
    value: "Mass Communication"
  },
  {
    label: "Psychology",
    value: "Psychology"
  },
  {
    label: "Physical Education",
    value: "Physical Education"
  },
  {
    label: "Political Science",
    value: "Political Science"
  },
  {
    label: "Sociology",
    value: "Sociology"
  },
  {
    label: "Sociology",
    value: "Sociology"
  },
  {
    label: "Social Work",
    value: "Social Work"
  }
];

export const options_department_theology = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "Sunni",
    value: "Sunni"
  },
  {
    label: "Shia",
    value: "Shia"
  }
];

export const options_department_unani = [
  {
    label: "Select Department",
    value: ""
  },
  {
    label: "All Departments of Unani",
    value: "All Departments of Unani"
  }
];

export const options_years = department_name => {
  let passingyearOptions;
  if (department_name !== "" && department_name !== undefined) {
    passingyearOptions = [
      {
        label: "Select Class",
        value: ""
      },
      {
        label: "Bachelor 1st year",
        value: "Bachelor 1st year"
      },
      {
        label: "Bachelor 2nd year",
        value: "Bachelor 2nd year"
      },
      {
        label: "Bachelor 3rd year",
        value: "Bachelor 3rd year"
      },
      {
        label: "Bachelor 4th year",
        value: "Bachelor 4th year"
      },
      {
        label: "Bachelor 5th year",
        value: "Bachelor 5th year"
      },
      {
        label: "Master 1st year",
        value: "Master 1st year"
      },
      {
        label: "Master 2nd year",
        value: "Master 2nd year"
      },
      {
        label: "Master 3rd year",
        value: "Master 3rd year"
      },
      {
        label: "Post Doctorate",
        value: "Post Doctorate"
      },
      {
        label: "Passout",
        value: "Passout"
      }
    ];
  } else {
    passingyearOptions = [
      {
        label: "Select Class",
        value: ""
      },
      {
        label: "Select Faculty and Department First",
        value: ""
      }
    ];
  }
  return passingyearOptions;
};

export const options_passingOut_year = [
  {
    label: "Select Year",
    value: ""
  },
  {
    label: "2018",
    value: "2018"
  },
  {
    label: "2017",
    value: "2017"
  },
  {
    label: "2016",
    value: "2016"
  },
  {
    label: "2015",
    value: "2015"
  },
  {
    label: "2014",
    value: "2014"
  },
  {
    label: "2013",
    value: "2013"
  },
  {
    label: "2012",
    value: "2012"
  }
];
