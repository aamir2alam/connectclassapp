import React from "react";
import { View, Dimensions } from "react-native";
import { Avatar, TouchableRipple } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
// import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";
import { Card, CardItem, Thumbnail, Body, Text, Button, H3, StyleProvider, Spinner } from "native-base";
import { Colors, Divider } from "react-native-paper";
import TimeAgo from "react-native-timeago";
import { baseurl } from "../../utils/baseUrl";
const { height, width } = Dimensions.get("window");
import getTheme from "../../../native-base-theme/components";
import material from "../../../native-base-theme/variables/material";
import isEmpty from "../../validation/is-empty";
import commonColor from "../../../native-base-theme/variables/commonColor";

/**
 *
 * @desc currently used to display both notes and notices
 */

const renderCatagory = data => {
  if (!isEmpty(data.item.target)) {
    switch (data.item.target) {
      case "class":
        return (
          <View>
            <Text style={styles.titleText}>{data.item.title}</Text>
            <Text style={{ backgroundColor: "#01579b", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
              Class
            </Text>
          </View>
        );
      case "department":
        return (
          <View>
            <Text style={styles.titleText}>{data.item.title}</Text>
            <Text style={{ backgroundColor: "#00695c", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
              Department
            </Text>
          </View>
        );
      case "faculty":
        return (
          <View>
            <Text style={styles.titleText}>{data.item.title}</Text>
            <Text style={{ backgroundColor: "#ff9800", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
              Faculty
            </Text>
          </View>
        );
      case "institution":
        return (
          <View>
            <Text style={styles.titleText}>{data.item.title}</Text>
            <Text style={{ backgroundColor: "#d50000", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
              Campus
            </Text>
          </View>
        );
      default:
        return (
          <View>
            <Text style={styles.titleText}>{data.item.title}</Text>
            <Text style={{ backgroundColor: "#00695C", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
              General
            </Text>
          </View>
        );
    }
  }
  switch (data.item.category) {
    case "PDF/Others":
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#c51162", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            {data.item.category}
          </Text>
        </View>
      );
    case "Syllabus":
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#ff9800", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            {data.item.category}
          </Text>
        </View>
      );
    case "Hand Written":
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#01579b", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            {data.item.category}
          </Text>
        </View>
      );
    case "Exam Papers":
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#00695c", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            {data.item.category}
          </Text>
        </View>
      );
    case "E-Book":
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#ff9800", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            {data.item.category}
          </Text>
        </View>
      );
    default:
      return (
        <View>
          <Text style={styles.titleText}>{data.item.title}</Text>
          <Text style={{ backgroundColor: "#3f51b5", color: "#ffffff", borderRadius: 4, width: "100%" }} note>
            General
          </Text>
        </View>
      );
  }
};
const NoticeCard = ({ data, onBookmark, onDownload, onProfileByHandle, loading2, loading_id }) => {
  return (
    <Card style={styles.card}>
      <CardItem header>
        <Body>{renderCatagory(data)}</Body>
      </CardItem>
      <CardItem>
        <Body>
          <Text style={{ marginTop: -22, color: "#212121", fontSize: 14 }}>{data.item.description}</Text>
        </Body>
      </CardItem>
      <CardItem>
        <Body style={{ flexDirection: "row", justifyContent: "space-between", marginTop: -10 }}>
          <TouchableRipple style={{ height: "120%", paddingTop: 15 }} onPress={onProfileByHandle}>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly", marginTop: -6 }}>
              <Thumbnail
                small
                style={{ width: 22, height: 22 }}
                source={{
                  uri: data.item.uploaded_by.profile_id.profile_picture
                }}
              />
              <Text style={{ marginLeft: 5, color: "#212121", fontSize: 12 }}>
                {data.item.uploaded_by.user_id.name}
              </Text>
              <Text note style={{ fontSize: 11, marginLeft: 3 }}>
                - <TimeAgo time={data.item.date} />
              </Text>
            </View>
          </TouchableRipple>
        </Body>
      </CardItem>
      <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
        {isEmpty(data.item.filename) ? (
          <Button transparent style={{ marginTop: -20 }}>
            <Icon active name="download" style={{ color: "#c9c9c9", fontSize: 22 }} />
          </Button>
        ) : (
          <Button transparent style={{ marginTop: -20 }} onPress={onDownload}>
            <Icon active name="download" style={{ color: "#0277bd", fontSize: 22 }} />
          </Button>
        )}
        {loading2 && data.item._id == loading_id ? (
          <Button transparent style={{ marginTop: -20 }} onPress={onBookmark}>
            <Spinner color={commonColor.blue800} size="small" />
          </Button>
        ) : (
          <Button transparent style={{ marginTop: -20 }} onPress={onBookmark}>
            <Icon
              active
              style={data.item.is_saved ? { color: "#0277bd", fontSize: 22 } : { color: Colors.grey600, fontSize: 22 }}
              name="bookmark"
            />
          </Button>
        )}
      </View>
    </Card>
  );
};

const styles = {
  card: {
    borderRadius: 4,
    elevation: 4,
    marginTop: -4,
    borderColor: "#9e9e9e",
    borderWidth: 0.5,
    boxShadow: "5 10px 20px 10px #000000"
  },
  titleText: {
    fontFamily: "Open Sans",
    fontWeight: "normal",
    fontSize: 20
  },
  descriptionText: { fontFamily: "Open Sans", fontWeight: "normal", fontSize: 12 },
  targetText: { fontFamily: "Open Sans", fontWeight: "normal", fontSize: 13 },
  avatarText: { fontFamily: "Open Sans", fontWeight: "normal", fontSize: 10 },
  timeText: { fontFamily: "Open Sans", fontWeight: "normal", fontSize: 8 }
};

export { NoticeCard };
