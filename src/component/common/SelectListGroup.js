import React from "react";
import { View, Picker } from "react-native";

const SelectListGroup = ({ name, placeholder, value, error, info, onValueChange, options }) => {
  const selectOptions = options.map(option => <Picker.Item label={option.label} value={option.value} />);
  return (
    <Picker selectedValue={value} style={{ height: 50, alignSelf: "stretch" }} onValueChange={onValueChange}>
      {selectOptions}
    </Picker>
  );
  //   return (
  //     <div>
  //       <div className="form-group">
  //         <select
  //           className={classnames("form-control", {
  //             "is-invalid": error
  //           })}
  //           name={name}
  //           value={value}
  //           onChange={onChange}
  //         >
  //           {selectOptions}>
  //         </select>
  //         {info && <small className="form-text text-muted">{info}</small>}
  //         {error && <div className="invalid-feedback">{error}</div>}
  //       </div>
  //     </div>
  //   );
};

export { SelectListGroup };
