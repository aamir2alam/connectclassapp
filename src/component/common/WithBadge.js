import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import { Badge } from "native-base";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from "react-redux";

class WithBadge extends PureComponent {
  render() {
    return (
      <View>
        <Badge primary style={{ position: "absolute", top: -8, right: -8, scaleX: 0.7, scaleY: 0.7 }}>
          <Text style={{ fontSize: 12, color: "#ffffff" }}>{this.props.count}</Text>
        </Badge>
        <Icon color={this.props.tintColor} name="bell-outline" size={24} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    count: state.notification.count
  };
};
export default connect(
  mapStateToProps,
  {}
)(WithBadge);
