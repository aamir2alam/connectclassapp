import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import PropTypes from "prop-types";
import { Text, View, StyleSheet, Image, Alert, ImageBackground } from "react-native";
import { Container, Content, Icon, Header, Body, List, ListItem, Left, Right, Button, Thumbnail } from "native-base";
import { DrawerActions } from "react-navigation";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { getCurrentProfile } from "../../actions/profileActions";
import isEmpty from "../../validation/is-empty";
import { baseurl } from "../../utils/baseUrl";
import commonColor from "../../../native-base-theme/variables/commonColor";

class DrawerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      errors: null,
      loading: false
    };
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  componentDidMount() {
    if (!isEmpty(this.props.profile)) {
      this.props.getCurrentProfile();
    }
  }
  componentWillReceiveProps(nextProps) {
    const { profile, loading } = this.props.profile;
    if (!isEmpty(nextProps)) {
      this.setState({
        profile: profile,
        loading: loading
      });
    }
  }

  onBookmarked = () => {
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.navigation.navigate("BookMarked");
    //  Actions.bookmarked();
  };

  onHome = () => {
    // Actions.navigator("Home");
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  onLogout = () => {
    Alert.alert(
      "Logout ?",
      "Are you sure to logout",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.props.logoutUser();
            this.props.navigation.navigate("Auth");
          }
        }
      ],
      { cancelable: true }
    );
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header style={styles.drawerHeader}>
          <ImageBackground
            source={{ uri: "https://storage.googleapis.com/lastcampus/default_images/profilecover.jpg" }}
            style={{
              resizeMode: "contain",
              height: 200,
              width: 200,
              alignItems: "center",
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            {isEmpty(this.state.profile) ? (
              <View>
                <Thumbnail
                  large
                  source={{
                    uri: "http://cdn.onlinewebfonts.com/svg/img_191958.png"
                  }}
                />
                <Text style={{ color: "#ffffff", fontSize: 18 }}>Loading..</Text>
              </View>
            ) : (
              <View style={{ alignItems: "center", flexDirection: "column", justifyContent: "center" }}>
                <Thumbnail
                  large
                  source={{
                    uri: this.state.profile.profile_picture
                  }}
                />
                <Button transparent>
                  <Text style={{ color: "#ffffff", fontSize: 18 }}>{this.state.profile.user.name}</Text>
                </Button>
                <Text style={{ color: "#ffffff", fontSize: 12, marginTop: -10 }}>
                  {this.state.profile.user.institution_name}
                </Text>
              </View>
            )}
          </ImageBackground>
        </Header>
        <Content>
          <List>
            {/* <ListItem avatar onPress={this.onHome}>
              <Left>
                <Icon name="home" size={24} style={{ color: commonColor.blue800 }} />
              </Left>
              <Body>
                <Text style={styles.menuItem}>Home</Text>
              </Body>
              <Right />
            </ListItem> */}
            <ListItem avatar onPress={this.onBookmarked}>
              <Left>
                <Icon name="bookmark" size={24} style={{ color: commonColor.blue800 }} />
              </Left>
              <Body>
                <Text style={styles.menuItem}>Saved items</Text>
              </Body>
              <Right />
            </ListItem>

            <ListItem avatar onPress={this.onLogout}>
              <Left>
                <Icon name="md-log-out" size={24} style={{ color: commonColor.blue800 }} />
              </Left>
              <Body>
                <Text style={styles.menuItem}>Logout</Text>
              </Body>
              <Right />
            </ListItem>
          </List>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  heading: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  drawerHeader: {
    height: 200
  },
  menuItem: {
    fontFamily: "Open Sans",
    fontWeight: "normal",
    fontSize: 16
  }
});

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

export default connect(
  mapStateToProps,
  { logoutUser, getCurrentProfile }
)(DrawerScreen);
