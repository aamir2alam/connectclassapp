import React from "react";
import ContentLoader from "rn-content-loader/src";
import { Circle, Rect } from "react-native-svg";
import { View } from "react-native";

const NotesLoader = () => {
  return (
    <View>
      <ContentLoader height={120} width={400}>
        <Rect x="21" y="2.11" rx="4" ry="4" width="320.58" height="14.34" />
        <Rect x="21" y="29" rx="3" ry="3" width="85" height="9.22" />

        <Circle cx="229.45" cy="70" r="13.9" />
        <Circle cx="100" cy="70" r="13.9" />
      </ContentLoader>
      <ContentLoader height={120} width={400}>
        <Rect x="21" y="2.11" rx="4" ry="4" width="320.58" height="14.34" />
        <Rect x="21" y="29" rx="3" ry="3" width="85" height="9.22" />

        <Circle cx="229.45" cy="70" r="13.9" />
        <Circle cx="100" cy="70" r="13.9" />
      </ContentLoader>
      <ContentLoader height={120} width={400}>
        <Rect x="21" y="2.11" rx="4" ry="4" width="320.58" height="14.34" />
        <Rect x="21" y="29" rx="3" ry="3" width="85" height="9.22" />

        <Circle cx="229.45" cy="70" r="13.9" />
        <Circle cx="100" cy="70" r="13.9" />
      </ContentLoader>
      <ContentLoader height={120} width={400}>
        <Rect x="21" y="2.11" rx="4" ry="4" width="320.58" height="14.34" />
        <Rect x="21" y="29" rx="3" ry="3" width="85" height="9.22" />

        <Circle cx="229.45" cy="70" r="13.9" />
        <Circle cx="100" cy="70" r="13.9" />
      </ContentLoader>
    </View>
  );
};

export { NotesLoader };
