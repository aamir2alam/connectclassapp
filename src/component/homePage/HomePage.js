import React, { PureComponent } from "react";
import { View, FlatList, RefreshControl, Alert, Platform } from "react-native";
import { DrawerActions } from "react-navigation";
import { connect } from "react-redux";
import Modal from "react-native-modalbox";
import RNFetchBlob from "react-native-fetch-blob";
import { PermissionsAndroid } from "react-native";
import {
  getStudentNotes,
  getTeacherNotes,
  bookmarkNotes,
  clearCurrentNotes,
  getStudentNotices
} from "../../actions/noteActions";
import { getProfileByHandle, getProfileById } from "../../actions/otherProfileActions";
import { getNewNotifications, getNotificationsCount } from "../../actions/notificationActions";
import { changeSwipeTab } from "../../actions/inAppActions/navigationActions";
import isEmpty from "../../validation/is-empty";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Right,
  Button,
  Content,
  Text,
  ListItem,
  Tabs,
  Tab,
  Spinner
} from "native-base";
import { styles } from "./styles";
import { NoticeCard } from "../common/NoticeCard";
import NoticePage from "../notice/NoticePage";
import commonColor from "../../../native-base-theme/variables/commonColor";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import BackgroundTimer from "react-native-background-timer";
import PushNotification from "react-native-push-notification";
import { NotesLoader } from "../common/NotesLoader";

class HomePage extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      pageno: 1,
      notes: [],
      currentPanel: "student",
      refreshing: false,
      loading: true,
      loading2: false,
      loading_id: "",
      hasMoreData: true,
      moreLoading: false,
      isDisabled: false,
      currrentTab: 0
    };
  }

  componentDidMount() {
    this._isMounted = true;
    //  this.props.getStudentNotes(this.state.pageno);
    this.props.getNotificationsCount();
    BackgroundTimer.setInterval(() => {
      // /   //code that will be called every 3 seconds
      this.props.getNotificationsCount();

      // this.props.getNewNotifications(1).then(res => {
      //   if (isEmpty(res)) {
      //     return;
      //   }
      //   this.generatePushNotifications(res);
      // });
    }, 5000);
    this.props.navigation.addListener("willFocus", route => {
      console.log(this.props.tabIndex);
      if (this.props.tabIndex === 0) {
        this.props.getStudentNotes(1);
      } else {
        this.props.getStudentNotices(1);
      }
      this.setState({
        currrentTab: this.props.tabIndex,
        refreshing: true
      });
    });
  }

  generatePushNotifications = list => {
    let title, msg;
    list.map(data => {
      if (data.reference.resource_type === "post") {
        return;
      }
      switch (data.event_type) {
        case "follow":
          (title = "You have new followers"), (msg = data.from_user.user_id.name + " Followed you");
          break;
        case "upload":
          (title = "You have new " + data.reference.resource_type),
            (msg = data.from_user.user_id.name + " uploaded new " + data.reference.resource_type + " in classroom");
          break;
        default:
          break;
      }

      PushNotification.presentLocalNotification({
        title: title,
        message: msg,
        category: "notes"
      });
    });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps) && nextProps.notes !== this.props.notes && this._isMounted) {
      this.setState({
        refreshing: false,
        moreLoading: false,
        loading: false
      });
      if (!nextProps.hasMoreData && this._isMounted) {
        this.setState({
          hasMoreData: false
        });
      }
    } else {
      if (this._isMounted)
        this.setState({
          refreshing: false,
          loading: false
        });
    }
  }

  handleRefresh = () => {
    switch (this.state.currentPanel) {
      case "student":
        this.setState(
          {
            refreshing: true
          },
          () => {
            this.props.getStudentNotes(1);
          }
        );
        break;
      case "teacher":
        this.setState(
          {
            refreshing: true
          },
          () => {
            this.props.getTeacherNotes(1);
          }
        );
        break;
    }
  };

  handleLoadMoreData = () => {
    if (!this.props.hasMoreData) {
      return;
    }
    this.setState({
      pageno: this.state.pageno + 1,
      moreLoading: true
    });
    if (this.state.currentPanel === "student") this.props.getStudentNotes(this.state.pageno + 1);
    else this.props.getTeacherNotes(this.state.pageno + 1);
  };

  onBars() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  openModal() {
    this.refs.modal.open();
  }
  onFilter(filter) {
    this.refs.modal.close();
    switch (filter) {
      case "student":
        if (this.state.currentPanel !== "student") {
          this.setState({
            refreshing: true,
            currentPanel: "student",
            pageno: 1
          });
          this.props.clearCurrentNotes();
          this.props.getStudentNotes(1);
        }
        break;
      case "teacher":
        if (this.state.currentPanel !== "teacher") {
          this.setState({
            refreshing: true,
            currentPanel: "teacher",
            pageno: 1
          });
          this.props.clearCurrentNotes();
          this.props.getTeacherNotes(1);
        }
        break;
    }
  }

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        title: "Storage Permission",
        message: "App needs access to your storage",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //permission granted
      } else {
        // permission denied
      }
    } catch (err) {
      console.warn(err);
    }
  }

  handleDownload = data => {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      .then(permission => {
        if (permission == true) {
          const { config, fs } = RNFetchBlob;
          let PictureDir = fs.dirs.PictureDir; // this is the pictures directory.

          let options = {
            fileCache: true,
            addAndroidDownloads: {
              useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
              notification: false,
              path: PictureDir + "/note_" + data.filename, // TODO: add original filename
              description: "Downloading Notes.."
            }
          };
          config(options)
            .fetch("GET", data.filename)
            .then(res => {
              //  Alert.alert("Success", "Note downloaded", [{ text: "OK" }]);
            });
        } else {
          this.requestStoragePermission();
        }
      })
      .catch(err => {
        Alert.alert("Failed", "Failed to downloaded the file", [{ text: "OK" }]);
      });
  };

  handleBookMark = item => {
    this.setState(
      {
        loading_id: item._id
      },
      () => {
        this.props.bookmarkNotes(item._id);
      }
    );
  };

  renderItem(item) {
    return (
      <NoticeCard
        style={{ elevation: 5, padding: 1, borderColor: "#000000" }}
        data={item}
        onBookmark={() => {
          this.handleBookMark(item.item);
        }}
        onProfileByHandle={() => {
          //Actions.other_profile();
          this.props.navigation.navigate("OtherProfile");
          if (!isEmpty(item.item.uploaded_by.profile_id.handle)) {
            this.props.getProfileByHandle(item.item.uploaded_by.profile_id.handle);
          } else {
            this.props.getProfileById(item.item.uploaded_by.profile_id._id);
          }
        }}
        onDownload={() => this.handleDownload(item.item)}
        loading_id={this.state.loading_id}
        loading2={this.props.loading2}
      />
    );
  }

  renderModal() {
    return (
      <Modal
        backdrop
        style={[styles.Modal, styles.modalWrapped]}
        position={"center"}
        ref={"modal"}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}
        isDisabled={this.state.isDisabled}
      >
        <Container style={{ alignSelf: "stretch" }}>
          <Header style={{ backgroundColor: "#f9f9f9", color: "#212121" }}>
            <Body>
              <Title style={{ color: "#212121" }}>Filters notes</Title>
            </Body>
          </Header>
          <Content>
            <ListItem icon onPress={() => this.onFilter("teacher")}>
              <Left>
                <Button style={{ backgroundColor: commonColor.blue800 }}>
                  <Icon color="#ffffff" name="school" size={20} />
                </Button>
              </Left>
              <Body>
                <Text>Teacher's notes</Text>
              </Body>
              <Right />
            </ListItem>
            <ListItem icon onPress={() => this.onFilter("student")}>
              <Left>
                <Button style={{ backgroundColor: commonColor.blue800 }}>
                  <Icon color="#ffffff" name="book" size={20} />
                </Button>
              </Left>
              <Body>
                <Text>All notes</Text>
              </Body>
              <Right />
            </ListItem>
          </Content>
        </Container>
      </Modal>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderModal()}
        <Header style={{ backgroundColor: "#f9f9f9", color: "#212121" }}>
          <Left>
            <Button transparent onPress={this.onBars.bind(this)}>
              <Icon name="menu" style={{ color: commonColor.blue800 }} size={20} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "#424242", alignSelf: "center" }}>Classroom</Title>
          </Body>
          <Right>
            {this.state.currrentTab === 1 ? (
              <View />
            ) : (
              <Button transparent onPress={this.openModal.bind(this)}>
                <Icon name="filter-variant" style={{ color: commonColor.blue800 }} size={20} />
              </Button>
            )}
          </Right>
        </Header>

        <View style={{ flex: 1, borderColor: "#ffffff" }}>
          <Tabs
            onChangeTab={({ i }) => {
              this.setState({ currrentTab: i });
              this.props.changeSwipeTab(i);
            }}
            tabBarUnderlineStyle={{ borderBottomWidth: 2, borderBottomColor: commonColor.blue900 }}
            initialPage={this.state.currrentTab}
            page={this.state.currrentTab}
            tabsContainerStyle={{ shadowColor: "white", shadowOpacity: 0 }}
            style={Platform.OS === "android" ? { overflow: "hidden" } : null}
          >
            <Tab
              tabStyle={{ backgroundColor: "#ffffff" }}
              activeTabStyle={{ backgroundColor: "#ffffff" }}
              textStyle={{ color: "#9e9e9e" }}
              activeTextStyle={{ color: "#000000" }}
              heading="Notes"
            >
              <View style={{ marginTop: 10, flex: 1 }}>
                {this.props.loading && !this.state.moreLoading ? (
                  <NotesLoader />
                ) : (
                  <FlatList
                    data={this.props.notes}
                    extraData={this.props.loading2}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => item._id}
                    initialNumToRender={7}
                    onEndReached={this.handleLoadMoreData}
                    refreshControl={
                      <RefreshControl
                        colors={["#0277BD"]}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                      />
                    }
                    onEndReachedThreshold={0.5}
                    ListHeaderComponent={
                      isEmpty(this.props.notes) ? (
                        <Text style={{ alignSelf: "center", marginTop: 15 }}>No notes available yet</Text>
                      ) : (
                        <View />
                      )
                    }
                    ListFooterComponent={this.props.loading ? <Spinner color={commonColor.blue800} /> : <View />}
                    style={{ marginTop: -6 }}
                  />
                )}
              </View>
            </Tab>
            <Tab
              tabStyle={{ backgroundColor: "#ffffff" }}
              activeTabStyle={{ backgroundColor: "#ffffff" }}
              textStyle={{ color: "#9e9e9e" }}
              activeTextStyle={{ color: "#000000" }}
              heading="Notices"
            >
              <NoticePage navigation={this.props.navigation} />
            </Tab>
          </Tabs>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    notes: state.notes.notes,
    note: state.notes.note,
    loading: state.notes.loading,
    loading2: state.notes.loading2,
    hasMoreData: state.notes.hasMoreData,
    auth: state.auth,
    errors: state.errors,
    count: state.notification.count,
    tabIndex: state.navigationState.tabIndex
  };
};

export default connect(
  mapStateToProps,
  {
    getStudentNotes,
    getTeacherNotes,
    bookmarkNotes,
    getProfileByHandle,
    getProfileById,
    clearCurrentNotes,
    getNewNotifications,
    getNotificationsCount,
    getStudentNotices,
    changeSwipeTab
  }
)(HomePage);
