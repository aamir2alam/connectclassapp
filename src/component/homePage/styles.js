import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  Modal: {
    justifyContent: "center",
    alignItems: "center",
    height: 200,
    width: 300,
    borderRadius: 5
  },
  modalWrapped: {
    marginBottom: 50
  }
});
