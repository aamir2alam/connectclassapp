import React, { PureComponent } from "react";
import { ImageBackground, StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { getAuthToken } from "../actions/authActions";
import isEmpty from "../validation/is-empty";

class Splash extends PureComponent {
  componentDidMount() {
    this.props.getAuthToken();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      setTimeout(function() {
        nextProps.navigation.navigate("App");
      }, 1000);
    } else {
      setTimeout(function() {
        nextProps.navigation.navigate("Auth");
      }, 1000);
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require("../img/splash.jpg")} style={styles.backgroundImage} />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "contain" // or 'stretch'
  }
});

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
export default connect(
  mapStateToProps,
  { getAuthToken }
)(Splash);
