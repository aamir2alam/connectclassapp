import React, { PureComponent } from "react";
import { Text, View, ImageBackground, ScrollView, Linking } from "react-native";
import { Title, IconButton, Colors, List, Divider, Subheading } from "react-native-paper";
import { PortfolioCard } from "./PortfolioCard";
import { styles } from "./styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Button, Thumbnail, Toast } from "native-base";
import { BackHandler } from "react-native";
import isEmpty from "../../validation/is-empty";

class MainProfilePage extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    //  this.props.navigation.goBack();
  };
  handleBack = () => {
    // this.props.navigation.goBack();
  };

  openDialer = phone => {
    if (isEmpty(phone)) {
      return;
    }
    Linking.canOpenURL(phone)
      .then(supported => {
        if (!supported) {
        } else {
          return Linking.openURL(phone);
        }
      })
      .catch(err => console.error("An error occurred", err));
  };
  openMail = email => {
    if (isEmpty(email)) {
      return;
    }
    Linking.canOpenURL(email)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: " + email);
        } else {
          return Linking.openURL(email);
        }
      })
      .catch(err => console.error("An error occurred", err));
  };

  showToast = msg => {
    Toast.show({
      text: msg,
      position: "bottom",
      duration: 2000
    });
  };

  renderPortfolio = type => {
    let is_item_present = false;

    let portfolioItem = this.props.profile.portfolio.map(item => {
      if (item.type == type) {
        is_item_present = true;
        return <PortfolioCard key={item._id} portfolio={item} />;
      }
    });

    if (!is_item_present) {
      return <View />;
    }

    // if (is_item_present == false && this.props.profile.user._id === this.props.auth.user.id) {
    //   return <Text style={{ alignSelf: "center" }}>Add {type}</Text>;
    // } else {
    //   return portfolioItem;
    // }
    switch (type) {
      case "education":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Education</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
      case "experience":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Experience</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
      case "internship":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Internships</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
      case "exam":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Exams</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
      case "paper":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Papers</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
      case "project":
        return (
          <View>
            <View>
              <Subheading style={styles.portfolioheading}>Projects</Subheading>
            </View>
            {portfolioItem}
          </View>
        );
    }
  };

  render() {
    const { profile, auth } = this.props;
    let is_current_user = profile.user._id === auth.user.id ? true : false;
    return (
      <ScrollView>
        <View>
          <View style={styles.cover}>
            <ImageBackground
              source={{ uri: "https://storage.googleapis.com/lastcampus/default_images/profilecover.jpg" }}
              style={{ width: "100%", height: "100%" }}
            />
          </View>
          <View style={styles.profilepic}>
            <Thumbnail
              circular
              scaleX={2}
              scaleY={2}
              style={{ margin: 30 }}
              source={{ uri: profile.profile_picture }}
            />
          </View>
          {!is_current_user ? (
            <View />
          ) : (
            <View style={styles.profileEdit}>
              <Button transparent onPress={() => this.props.navigation.navigate("EditPage")}>
                <Icon name="pencil" size={18} style={{ color: "#0277bd" }} />
              </Button>
            </View>
          )}
          <View>
            <Title style={styles.title}>{profile.user.name}</Title>
          </View>
          {!isEmpty(profile.handle) ? (
            <View>
              <Text style={[styles.title, { fontSize: 10, marginTop: -2 }]}>{"@" + profile.handle}</Text>
            </View>
          ) : (
            <View style={{ margin: 0, padding: 0 }} />
          )}
          {!isEmpty(profile.bio) ? (
            <View>
              <Text style={[styles.title, { marginTop: 10, marginBottom: -4 }]}>{profile.bio}</Text>
            </View>
          ) : (
            <View style={{ margin: 0, padding: 0 }} />
          )}
          <View>
            <Text style={styles.batch}>
              {profile.department_name} - {profile.passing_year}
            </Text>
          </View>
          <View style={styles.callmailpanel}>
            <IconButton
              icon="phone"
              color={Colors.blue500}
              size={20}
              onPress={() =>
                !isEmpty(profile.phoneno)
                  ? this.openDialer(`tel:+91${profile.phoneno}`)
                  : this.showToast("Phone is not added by user")
              }
            />
            <IconButton
              icon="mail"
              color={Colors.blue500}
              size={20}
              onPress={() =>
                !isEmpty(profile.visible_email)
                  ? this.openMail(`mailto:${profile.visible_email}`)
                  : this.showToast("Email is not added by user")
              }
            />
          </View>
          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="school" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Institute: </Text>
                <Text>{profile.user.institution_name}</Text>
              </Text>
            </View>
          </View>

          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="clipboard" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Faculty: </Text>
                <Text>{profile.faculty_name}</Text>
              </Text>
            </View>
          </View>

          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="book" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Department: </Text>
                <Text>{profile.department_name} </Text>
              </Text>
            </View>
          </View>

          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="account-group" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Batch: </Text>
                <Text>{profile.passing_year} </Text>
              </Text>
            </View>
          </View>

          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="home" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Home: </Text>
                <Text>{profile.location_home}</Text>
              </Text>
            </View>
          </View>

          <View style={styles.about}>
            <View>
              <Icon style={{ marginLeft: 15, marginRight: 10 }} name="map-marker" size={14} />
            </View>
            <View>
              <Text>
                <Text style={styles.aboutheading}>Current City: </Text>
                <Text>{profile.location_current}</Text>
              </Text>
            </View>
          </View>
          <Divider />
          {!isEmpty(this.props.profile.portfolio) ? (
            <View />
          ) : !is_current_user ? (
            <View />
          ) : (
            <View style={{ margin: 10 }}>
              <Text>
                Your portfolio is empty, add your portfolio details to make it outstanding.{" "}
                {console.log(is_current_user)}
              </Text>
            </View>
          )}
          {this.renderPortfolio("education")}
          <Divider />
          {this.renderPortfolio("experience")}
          <Divider />
          {this.renderPortfolio("internship")}
          <Divider />
          {this.renderPortfolio("paper")}
          <Divider />
          {this.renderPortfolio("exam")}
          <Divider />
          {this.renderPortfolio("project")}
        </View>
      </ScrollView>
    );
  }
}

export default MainProfilePage;
