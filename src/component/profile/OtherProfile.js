import React, { PureComponent } from "react";
import { connect } from "react-redux";
import isEmpty from "../../validation/is-empty";
import MainProfilePage from "./MainProfilePage";
import { Container, Content } from "native-base";
import { ProfileLoader } from "./ProfileLoader";

class OtherProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      errors: null,
      loading: false
    };
  }
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    const { profile, loading } = this.props.profile;

    if (!isEmpty(nextProps)) {
      this.setState({
        profile: profile,
        loading: loading
      });
    }
  }

  render() {
    const { profile, loading } = this.props.profile;
    let profileContent;
    if (isEmpty(profile)) {
      profileContent = <ProfileLoader />;
    } else {
      profileContent = <MainProfilePage profile={profile} auth={this.props.auth} />;
    }
    return (
      <Container>
        <Content>{profileContent}</Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.other_profile,
    errors: state.errors,
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  {}
)(OtherProfile);
