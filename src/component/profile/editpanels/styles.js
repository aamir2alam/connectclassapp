export const styles = {
  errorStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red"
  },
  popbar: {
    marginTop: 90,
    textAlign: "center"
  },
  headtitle: {
    color: "#0077b5",
    fontSize: 30,
    marginTop: 160,
    fontWeight: "700",
    textAlign: "center"
  },

  inputbox: {
    backgroundColor: "transparent",
    border: 0,
    color: "#f2f2f2",
    fontSize: 20,
    height: 60,
    lineHeight: 30,
    outline: "none",
    width: "100%"
  },
  buttons: {
    backgroundColor: "#0077b5",
    radius: "5%",
    margin: 10
  },
  buttonsignup: {
    backgroundColor: "transparent"
  },
  dateIcon: {
    position: "absolute",
    left: 0,
    top: 4,
    marginLeft: 0
  },
  dateInput: {
    marginLeft: 36
  }
};
