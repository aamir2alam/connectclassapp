import React, { PureComponent } from "react";
import { ScrollView, TouchableNativeFeedback, View } from "react-native";
import { BackHandler } from "react-native";
//import { DatePicker } from "react-native-datepicker";
import { styles } from "./styles";
import { Card, Title, TextInput } from "react-native-paper";
import { connect } from "react-redux";
import { createProfile } from "../../../actions/profileActions";
import isEmpty from "../../../validation/is-empty";
import {
  options_faculty,
  options_department,
  options_years,
  options_passingOut_year
} from "../../common/faculty_department_passing_year";
import { SelectListGroup } from "../../common/SelectListGroup";
import { Header, Left, Button, Body, Right, Container, Content, Thumbnail, DatePicker, Toast } from "native-base";
import ImagePicker from "react-native-image-picker";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import commonColor from "../../../../native-base-theme/variables/commonColor";

class CreateAbout extends PureComponent {
  constructor(props) {
    super(props);
    const { profile } = this.props;

    this.state = {
      handle: profile.handle,
      phoneno: profile.phoneno,
      date_of_birth: !isEmpty(profile.date_of_birth) ? profile.date_of_birth : "",
      visible_email: profile.visible_email,
      location_current: profile.location_current,
      location_home: profile.location_home,
      faculty_name: profile.faculty_name,
      department_name: profile.department_name,
      passing_year: profile.passing_year,
      bio: profile.bio,
      profile_picture: !isEmpty(profile.profile_picture) ? profile.profile_picture : "",
      profile_pic_data: {},
      passingout_year: profile.passingout_year,
      facebook: !isEmpty(profile.social) ? profile.social.facebook : "",
      twitter: !isEmpty(profile.social) ? profile.social.twitter : "",
      linkedin: !isEmpty(profile.social) ? profile.social.linkedin : "",
      instagram: !isEmpty(profile.social) ? profile.social.instagram : "",
      youtube: !isEmpty(profile.social) ? profile.social.youtube : "",
      errors: {},
      is_image_picked: false
    };

    this.handleBackPress = this.handleBackPress.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {};
  onSubmit() {
    const profileFormData = new FormData();
    let flag = false;

    if (this.state.handle != this.props.profile.handle && !isEmpty(this.state.handle)) {
      profileFormData.append("handle", this.state.handle);
      flag = true;
    }

    if (this.state.visible_email != this.props.profile.visible_email && !isEmpty(this.state.visible_email)) {
      profileFormData.append("visible_email", this.state.visible_email);
      flag = true;
    }
    if (this.state.date_of_birth !== this.props.profile.date_of_birth && this.state.date_of_birth != undefined) {
      profileFormData.append("date_of_birth", this.state.date_of_birth);
      flag = true;
    }

    if (this.state.phoneno != this.props.profile.phoneno && !isEmpty(this.state.phoneno)) {
      profileFormData.append("phoneno", this.state.phoneno);
      flag = true;
    }
    if (this.state.faculty_name !== this.props.profile.faculty_name && !isEmpty(this.state.faculty_name)) {
      profileFormData.append("faculty_name", this.state.faculty_name);
      flag = true;
    }
    if (this.state.passing_year != this.props.profile.passing_year && !isEmpty(this.state.passing_year)) {
      profileFormData.append("passing_year", this.state.passing_year);
      flag = true;
    }
    if (this.state.passingout_year != this.props.profile.passingout_year && !isEmpty(this.state.passingout_year)) {
      profileFormData.append("passingout_year", this.state.passingout_year);
      flag = true;
    }
    if (this.state.department_name != this.props.profile.department_name && !isEmpty(this.state.department_name)) {
      profileFormData.append("department_name", this.state.department_name);
      flag = true;
    }
    if (this.state.bio != this.props.profile.bio && !isEmpty(this.state.bio)) {
      profileFormData.append("bio", this.state.bio);
      flag = true;
    }
    if (this.state.location_current != this.props.profile.location_current && !isEmpty(this.state.location_current)) {
      profileFormData.append("location_current", this.state.location_current);
      flag = true;
    }
    if (this.state.location_home != this.props.profile.location_home && !isEmpty(this.state.location_home)) {
      profileFormData.append("location_home", this.state.location_home);
      flag = true;
    }
    if (this.state.profile_picture !== this.props.profile.profile_picture && !isEmpty(this.state.profile_picture)) {
      profileFormData.append(
        "profile_picture",
        {
          uri: this.state.profile_pic_data.uri,
          name: this.state.profile_pic_data.fileName,
          type: this.state.profile_pic_data.type
        },
        this.state.profile_pic_data.fileName
      );
      flag = true;
    }

    //TODO: social fields are not updating yet
    if (!flag) {
      this.props.navigation.goBack();
      return;
    }
    this.props.createProfile(profileFormData);
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps) && nextProps.profile != this.props.profile) {
      this.props.navigation.goBack();
    } else {
      this.props.navigation.goBack();
    }
  }

  handleBack = () => {
    this.props.navigation.goBack();
  };

  handleImagePicker = () => {
    const options = {
      title: "Select Profie picture",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
        return;
      } else if (response.error) {
        console.log("ImagePicker Error: ", response);
        return;
      } else {
        const source = { uri: response.uri };
        if (isEmpty(source)) {
          return;
        }
        console.log(response);
        this.setState({
          profile_picture: response.uri,
          is_image_picked: true,
          profile_pic_data: response
        });
      }
    });
  };
  setDate = newDate => {
    this.setState({ date_of_birth: newDate.toString() });
  };
  render() {
    const { errors } = this.props;

    return (
      <Container>
        <Header style={{ backgroundColor: "#f9f9f9" }}>
          <Left>
            <Button transparent onPress={this.handleBack.bind(this)}>
              <Icon name="keyboard-backspace" size={20} style={{ color: commonColor.blue800 }} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "#212121" }}>Edit Profile</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.onSubmit.bind(this)}>
              <Icon name="check" size={20} style={{ color: commonColor.blue800 }} />
            </Button>
          </Right>
        </Header>
        <Content>
          <ScrollView style={{ marginBottom: 25 }}>
            <Card style={{ marginTop: 5 }}>
              <TouchableNativeFeedback onPress={this.handleImagePicker}>
                <Thumbnail
                  circular
                  scaleX={2}
                  scaleY={2}
                  style={{ margin: 30, alignSelf: "center" }}
                  source={{
                    uri: this.state.profile_picture
                  }}
                />
              </TouchableNativeFeedback>
              <Title style={{ marginTop: 10, paddingLeft: 10, fontSize: 18 }}>Basic info</Title>
              <TextInput
                label="Username"
                // mode="outlined"
                // theme={{ colors: { placeholder: "grey", background: "#f5f6f5", text: "grey", primary: "#5d5d5d" } }}
                onChangeText={handle => this.setState({ handle })}
                value={this.state.handle}
                style={styles.inputbox}
              />
              <TextInput
                label="Email"
                // mode="outlined"
                placeholder="Enter your email"
                onChangeText={visible_email => this.setState({ visible_email })}
                value={this.state.visible_email}
                style={styles.inputbox}
              />
              <TextInput
                label="Mobile number"
                placeholder="10 digit mobile number "
                // mode="outlined"
                keyboardType="numeric"
                onChangeText={phoneno => this.setState({ phoneno })}
                value={this.state.phoneno}
                style={styles.inputbox}
              />
              <View style={{ flexDirection: "row" }}>
                <Title style={{ marginLeft: 12, fontSize: 14 }}>Date of birth</Title>
                <DatePicker
                  defaultDate={!isEmpty(this.state.date_of_birth) ? new Date(this.state.date_of_birth) : new Date()}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderTextStyle={{ color: "#212121" }}
                  onDateChange={this.setDate}
                  disabled={false}
                />
              </View>
              <TextInput
                label="Current city"
                placeholder="Enter your current city"
                onChangeText={location_current => this.setState({ location_current })}
                value={this.state.location_current}
                style={styles.inputbox}
              />
              <TextInput
                label="Home city"
                placeholder="Enter your home city"
                onChangeText={location_home => this.setState({ location_home })}
                value={this.state.location_home}
                style={styles.inputbox}
              />
              <TextInput
                label="Bio"
                placeholder="Write about yourself"
                numberOfLines={2}
                onChangeText={bio => this.setState({ bio })}
                value={this.state.bio}
                style={styles.inputbox}
              />
              <Title style={{ marginTop: 10, paddingLeft: 10, fontSize: 18 }}>Institutional info</Title>

              <SelectListGroup
                options={options_faculty}
                value={this.state.faculty_name}
                onValueChange={faculty_name => this.setState({ faculty_name })}
              />
              <SelectListGroup
                options={options_department(this.state.faculty_name)}
                value={this.state.department_name}
                onValueChange={department_name => this.setState({ department_name })}
              />
              <SelectListGroup
                options={options_years(this.state.department_name)}
                value={this.state.passing_year}
                onValueChange={passing_year => this.setState({ passing_year })}
              />
              {this.state.passing_year === "Passout" ? (
                <TextInput
                  label="Passing Out"
                  value={this.state.passingout_year != undefined ? this.state.passingout_year : "2018"}
                  onChange={passingout_year => this.setState({ passingout_year })}
                  type="number"
                />
              ) : (
                <Content />
              )}
            </Card>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  const { profile, loading2, progress } = state.profile;

  return { profile, loading2, progress };
};

export default connect(
  mapStateToProps,
  { createProfile }
)(CreateAbout);
