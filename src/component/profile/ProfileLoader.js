import React from "react";
import ContentLoader from "rn-content-loader/src";
import { Circle, Rect } from "react-native-svg";
import { View } from "react-native";

const ProfileLoader = () => {
  return (
    <View style={{ alignItems: "center", marginTop: 5 }}>
      <ContentLoader height={100}>
        <Circle cx="150" cy="60" r="60" />
      </ContentLoader>
      <ContentLoader height={100}>
        <Rect x="20" y="13" rx="4" ry="4" width="300" height="13" />
        <Rect x="20" y="37" rx="4" ry="4" width="300" height="13" />
      </ContentLoader>
      <ContentLoader height={70}>
        <Rect x="20" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="205" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={70}>
        <Rect x="20" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="20" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={70}>
        <Rect x="20" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="20" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
      <ContentLoader height={70}>
        <Rect x="20" y="13" rx="4" ry="4" width="400" height="13" />
        <Rect x="20" y="37" rx="4" ry="4" width="400" height="13" />
      </ContentLoader>
    </View>
  );
};

export { ProfileLoader };
