import React from "react";
import { Text, View, ImageBackground, ScrollView } from "react-native";
import { Title, Avatar, IconButton, Colors, List, Divider, Subheading } from "react-native-paper";
import { styles } from "./styles";
import isEmpty from "../../validation/is-empty";
import { Thumbnail } from "native-base";

const PortfolioCard = props => {
  const { heading, subheading, from, to, description, image, url, _id } = props.portfolio;

  const toYear = new Date(to);
  const toYearDisplay = to !== undefined && to !== null ? toYear.getFullYear() : "";
  const fromYear = new Date(from);
  const fromYearDisplay = from !== undefined && from !== null ? fromYear.getFullYear() : "";

  const date = (
    <Text>
      {toYear.getFullYear()}-{fromYear.getFullYear()}
    </Text>
  );

  return (
    <View>
      <List.Item
        title={heading}
        description={
          !isEmpty(subheading)
            ? subheading
            : "" + !isEmpty(description)
            ? "" + description
            : "" + !isEmpty(fromYearDisplay)
            ? " ( " + fromYearDisplay
            : "" + !isEmpty(toYearDisplay)
            ? "- " + toYearDisplay + " ) "
            : ""
        }
        key={_id}
        left={props => <Thumbnail size={22} circular={true} source={{ uri: image }} />}
      />
    </View>
  );
};

export { PortfolioCard };
