export const styles = {
  cover: {
    height: 100
  },
  profilepic: {
    marginTop: -50,

    flexDirection: "row",
    justifyContent: "center"
  },
  profileEdit: {
    marginTop: -70,
    marginBottom: 30,
    marginRight: 15,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  profileavatar: {
    backgroundColor: "transparent",
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#ffffff"
  },
  title: {
    textAlign: "center",
    marginBottom: 0,
    paddingBottom: 0
  },
  batch: {
    marginTop: 10,
    textAlign: "center",
    color: "#666666"
  },
  callmailpanel: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10
  },
  about: {
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingBottom: 10
  },
  aboutheading: {
    fontWeight: "600",
    flexBasis: "50%"
  },
  portfolioheading: {
    fontWeight: "600",
    marginLeft: 10
  },
  listavatar: {
    marginTop: 8,
    backgroundColor: "transparent"
  },
  logoutModal: {
    justifyContent: "center",
    alignItems: "center",
    height: 150,
    width: 300
  },
  modalWrapped: {
    marginBottom: 50
  }
};
