import React, { PureComponent } from "react";
import { connect } from "react-redux";
import isEmpty from "../../validation/is-empty";
import { getCurrentProfile, getProfileByHandle } from "../../actions/profileActions";
import { logoutUser } from "../../actions/authActions";
import MainProfilePage from "./MainProfilePage";
import { Container, Button, Content, Text, Grid, Row, Col, Toast } from "native-base";
import { styles } from "./styles";
import Modal from "react-native-modalbox";
import { ProfileLoader } from "./ProfileLoader";

class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      errors: null,
      loading: false,
      isOpen: false,
      isDisabled: false,
      showToast: false
    };
  }
  componentDidMount() {
    this.setState({
      loading: true
    });
    if (isEmpty(this.props.profile)) this.props.getCurrentProfile();
  }

  onEdit() {
    this.props.navigation.navigate("EditProfile");
  }

  onLogout() {
    //this.refs.modal.open();
    //this.props.logoutUser();
  }

  componentWillReceiveProps(nextProps) {
    const { profile, loading } = this.props.profile;

    if (!isEmpty(nextProps)) {
      this.setState({
        profile: profile,
        loading: loading
      });
    }
    if (this.props.progress > 0 && this.props.progress < 100) {
      Toast.show({
        text: "Updating profile picture " + this.props.progress + " %",
        position: "top",
        duration: 3000
      });
    }
  }

  renderModal() {
    return (
      <Modal
        backdrop
        style={[styles.logoutModal, styles.modalWrapped]}
        position={"center"}
        ref={"modal"}
        isDisabled={this.state.isDisabled}
      >
        <Grid>
          <Row size={1}>
            <Text>Logout account ?</Text>
          </Row>
          <Row size={3}>
            <Col>
              <Button
                onPress={() => {
                  this.props.logoutUser();
                  this.refs.modal.close();
                }}
              >
                <Text>Yes</Text>
              </Button>
            </Col>
            <Col>
              <Button onPress={() => this.refs.modal.close()}>
                <Text>No</Text>
              </Button>
            </Col>
          </Row>
        </Grid>
      </Modal>
    );
  }

  render() {
    const { profile, loading } = this.props.profile;

    console.log(profile);
    let profileContent;
    if (isEmpty(profile)) {
      profileContent = <ProfileLoader />;
    } else {
      profileContent = (
        <MainProfilePage
          profile={profile}
          auth={this.props.auth}
          progress={this.props.progress}
          navigation={this.props.navigation}
        />
      );
    }
    return (
      <Container>
        <Content>{profileContent}</Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile,
    errors: state.errors,
    auth: state.auth,
    progress: state.profile.progress
  };
};

export default connect(
  mapStateToProps,
  { getCurrentProfile, getProfileByHandle, logoutUser }
)(Profile);
