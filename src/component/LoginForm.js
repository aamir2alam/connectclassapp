import React, { PureComponent } from "react";
import { Card, Title, Button, TextInput, Text } from "react-native-paper";
import { View, Image, KeyboardAvoidingView, Dimensions, Linking } from "react-native";
import { connect } from "react-redux";
import { loginUser, getAuthToken, clearAuthErrors } from "../actions/authActions";
import isEmpty from "../validation/is-empty";
import { Input, Item, Spinner } from "native-base";
import commonColor from "../.././native-base-theme/variables/commonColor";
const window = Dimensions.get("window");

const IMAGE_HEIGHT = window.width / 2;
const IMAGE_HEIGHT_SMALL = window.width / 7;

class LoginForm extends PureComponent {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getAuthToken();
  }
  componentWillUnmount() {
    if (this.props.errors) this.props.clearAuthErrors();
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps) && nextProps.auth.isAuthenticated) {
      this.props.navigation.navigate("App");
    }

    if (!isEmpty(nextProps) && nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  renderError() {
    console.log(this.props.errors);
    if (this.props.errors.email || this.props.errors.password) {
      return (
        <View>
          <Text style={styles.errorStyle}>{this.props.errors.email}</Text>
          <Text style={styles.errorStyle}>{this.props.errors.password}</Text>
        </View>
      );
    }
  }

  onLogin() {
    if (!isEmpty(this.props.errors)) {
      this.props.clearAuthErrors();
    }
    if (isEmpty(this.state.email) || isEmpty(this.state.password)) {
      alert("Please enter both email and password");
      return;
    }
    this.props.loginUser({ email: this.state.email, password: this.state.password });
    // this.props.navigation.navigate("App");
  }
  onRegister() {
    if (!isEmpty(this.props.errors)) {
      this.props.clearAuthErrors();
    }
    Linking.openURL("https://lastcampus.com/register");
  }

  renderButton() {
    return (
      <View>
        {!this.props.loading ? (
          <Button mode="contained" style={styles.buttons} onPress={this.onLogin.bind(this)}>
            <Text style={styles.buttonText}>Log in</Text>
          </Button>
        ) : (
          <Spinner color={commonColor.blue800} />
        )}
        <Button mode="text" color="#0077b5" style={styles.buttonsignup} onPress={this.onRegister.bind(this)}>
          <Text>Sign up</Text>
        </Button>
      </View>
    );
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View>
          <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 80 }}>
            <Image
              source={{ uri: "https://storage.googleapis.com/lastcampus/default_images/LastCampusLogo.png" }}
              style={{ width: "50%", height: 60, resizeMode: "contain" }}
            />
          </View>
          <Text style={{ alignSelf: "center", fontSize: 22, marginTop: 15 }}>Sign in</Text>
          <Text style={{ alignSelf: "center", marginTop: 5 }}>to continue to LastCampus</Text>

          <View style={{ marginTop: 50 }}>
            <Item regular style={{ margin: 10, marginLeft: 10 }}>
              <Input
                label="Email"
                placeholder="Email"
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
                style={styles.inputbox}
              />
            </Item>
            <Item regular style={{ margin: 10, marginLeft: 10 }}>
              <Input
                secureTextEntry
                label="Password"
                placeholder="Password"
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                style={styles.inputbox}
              />
            </Item>
          </View>

          {this.renderError()}

          {this.renderButton()}
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = {
  errorStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red"
  },
  popbar: {
    marginTop: 90,
    textAlign: "center"
  },
  headtitle: {
    color: "#0077b5",
    fontSize: 30,
    marginTop: 10,
    fontWeight: "700",
    textAlign: "center"
  },

  inputbox: {
    backgroundColor: "transparent",

    color: "#000000",
    fontSize: 18,
    height: 50,
    lineHeight: 30,

    width: "100%"
  },
  buttons: {
    backgroundColor: "#01579b",
    radius: "8%",
    margin: 15
  },
  buttonText: {
    color: "#ffffff"
  },
  buttonsignup: {
    backgroundColor: "transparent"
  },
  container: {
    justifyContent: "center"
  }
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors,
    loading: state.auth.loading
  };
};

export default connect(
  mapStateToProps,
  { loginUser, getAuthToken, clearAuthErrors }
)(LoginForm);
