import React, { PureComponent } from "react";
import { Card, Title, Button, TextInput, Text } from "react-native-paper";
import {
  View,
  KeyboardAvoidingView,
  Dimensions,
  Image,
  ListView,
  TouchableNativeFeedback,
  TouchableHighlight
} from "react-native";
import { Spinner } from "../component/common/Spinner";
import { connect } from "react-redux";
import { registerUser, clearAuthErrors } from "../actions/authActions";
import { getInstitutionSuggestions } from "../actions/registerActions";
import { Container, Header, Content, Input, Item } from "native-base";
import isEmpty from "../validation/is-empty";
import Autocomplete from "react-native-autocomplete-input";
import Axios from "axios";

const API = "https://swapi.co/api";
const ROMAN = ["", "I", "II", "III", "IV", "V", "VI", "VII"];

class RegisterForm extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      errors: {},
      institutions: [],
      query: "",
      institution_name: ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      //TODO: handle after register
    }
  }
  componentDidMount() {
    if (!isEmpty(this.props.errors)) {
      clearAuthErrors();
    }
  }
  componentWillUnmount() {
    this.props.clearAuthErrors();
  }
  renderError() {
    const { name, email, password, password2, institution_name } = this.props.errors;

    if (name || email || password || password2 || institution_name) {
      return (
        <View>
          <Text style={styles.errorStyle}>{name}</Text>
          <Text style={styles.errorStyle}>{email}</Text>
          <Text style={styles.errorStyle}>{password}</Text>
          <Text style={styles.errorStyle}>{password2}</Text>
          <Text style={styles.errorStyle}>{institution_name}</Text>
        </View>
      );
    }
  }

  onRegister() {
    const { name, email, password, password2 } = this.state;
    if (isEmpty(name) || isEmpty(email) || isEmpty(password) || isEmpty(password2)) {
      alert("Please fill all fileds");
      return;
    }
    this.props.registerUser({
      name: name,
      email: email,
      password: password,
      password2: password2,
      institution_name: "Aligarh Muslim University"
    });
  }

  renderButton() {
    if (this.props.auth.loading) {
      return <Spinner size="large" />;
    }
    return (
      <Button mode="contained" style={styles.buttons} onPress={this.onRegister.bind(this)}>
        Sign up
      </Button>
    );
  }

  fetchSuggestions = query => {
    let obj = {
      name: query
    };
    this.props
      .getInstitutionSuggestions(obj)
      .then(res => {
        console.log(this.state.institutions);
        this.setState({
          institutions: res
        });
      })
      .catch(err => {});
  };

  onListSelect = name => {
    this.setState({
      institution_name: name,
      institutions: []
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View>
          <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 100 }}>
            <Image
              source={{ uri: "https://storage.googleapis.com/lastcampus/default_images/LastCampusLogo.png" }}
              style={{ width: "60%", height: 60 }}
            />
          </View>

          <View style={{ marginTop: 50 }}>
            <Item regular style={{ margin: 10, marginLeft: 20 }}>
              <Input
                label="Full name"
                placeholder="Full name"
                onChangeText={name => this.setState({ name })}
                value={this.state.name}
                style={styles.inputbox}
              />
            </Item>
            <Item regular style={{ margin: 10, marginLeft: 20 }}>
              <Input
                label="Email"
                placeholder="Email"
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
                style={styles.inputbox}
              />
            </Item>
            <Item regular style={{ margin: 10, marginLeft: 20 }}>
              <Autocomplete
                autoCapitalize="none"
                autoCorrect={false}
                containerStyle={styles.inputboxSuggestion}
                data={this.state.institutions}
                defaultValue={this.state.institution_name}
                onChangeText={this.fetchSuggestions}
                placeholder="Institution name"
                renderItem={({ name }) => (
                  <TouchableNativeFeedback onPress={() => this.onListSelect(name)}>
                    <Text style={styles.itemTextSeggestions}>{name}</Text>
                  </TouchableNativeFeedback>
                )}
              />
            </Item>
            <Item regular style={{ margin: 10, marginLeft: 20 }}>
              <Input
                secureTextEntry
                label="Password"
                placeholder="Password"
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                style={styles.inputbox}
              />
            </Item>
            <Item regular style={{ margin: 10, marginLeft: 20 }}>
              <Input
                secureTextEntry
                label="Confirm Password"
                placeholder="Confirm password"
                onChangeText={password2 => this.setState({ password2 })}
                value={this.state.password2}
                style={styles.inputbox}
              />
            </Item>
          </View>

          {this.renderError()}

          {this.renderButton()}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  errorStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red"
  },
  popbar: {
    marginTop: 90,
    textAlign: "center"
  },
  headtitle: {
    color: "#0077b5",
    fontSize: 30,
    marginTop: 10,
    fontWeight: "700",
    textAlign: "center"
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  itemTextSeggestions: {
    fontSize: 15,
    margin: 2,
    height: 40
  },
  inputbox: {
    backgroundColor: "transparent",
    borderColor: "#e5e5e5",
    borderWidth: 1,
    color: "#000000",
    fontSize: 18,
    height: 50,
    lineHeight: 30,
    borderRadius: 7,
    width: "100%"
  },
  inputboxSuggestion: {
    backgroundColor: "transparent",
    borderColor: "#e5e5e5",
    borderWidth: 1,
    color: "#000000",
    fontSize: 18,
    minHeight: 50,
    lineHeight: 30,
    borderRadius: 7,
    width: "100%"
  },
  buttons: {
    backgroundColor: "#01579b",
    radius: "8%",
    margin: 15
  },
  buttonsignup: {
    backgroundColor: "transparent"
  },
  container: {
    justifyContent: "center"
  }
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors
  };
};

export default connect(
  mapStateToProps,
  { registerUser, clearAuthErrors, getInstitutionSuggestions }
)(RegisterForm);
